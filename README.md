## PBADN: Pattern-Based Anomaly Detection in a Network of Multivariate Time Series

Implementation of  _Pattern-Based Anomaly Detection in a Network of Multivariate Time Series_ , 
by Len Feremans, Boris Cule and Bart Goethhals

Paper submitted to the ACM International Conference on Information and Knowledge Management (CIKM ’22).

> Recent technological developments make it commonplace to have a complex system of entities such as servers or industrial devices in a network. In this context, detecting abnormal periods of operation in sensors, entities or communication has become a crucial data mining task. State-of-the-art methods based on unsupervised deep-learning methods are accurate but at the cost of significant training time and comprehension as patterns of common behaviour and relations between sensors are obscured in the multi-layer neural network. Our goal is to provide an efficient and interpretable model where patterns of common behaviour and intra- and inter-device relationships are explicit for the end-user. We capture the normal behaviour of time series using frequent and cohesive sequential patterns that compress the data best using minimum description length. For computing anomalies, we construct a pattern-based embedding of time series of different types and use an isolation forest to make predictions. Finally, we propose a method to discover similar time series based on density-based fingerprints and histograms that scale to large collections of time series. We study the properties of our method through experiments on public datasets, as well as on proprietary data provided by a telecom company, thus demonstrating its speed, scalability, interpretability and high anomaly detection performance compared to state-of-the-art methods.

### Summary

**PBADN** takes a network of devices as input, where each devices consist of one or more time series.
**PBADN** computes an anomaly score for each window without the need for *labels* and high accuracy and performance on both univariate, multivatiate time series.
For network dataset, we first reverse-engineer the network and than add similar time series to an existing device.

**PBADN** consist of 4 major parts:

1. A scalable and accurate method to identify similar time series in a large network, i.e. about 8 minutes is required for comparing all pairs in a network of 8000 time series
2. A method to discover frequent, cohesive and sequential  patterns that compress the data best defined using minimum description length
3. A method that compute an anomaly based on either an isolation forest or frequent pattern outlier factors based on an embedding of patterns
4. Extension to handle multivariate time series, or entities, and context-aware (or network-aware) anomaly detection.
 
### Tutorial ###

See [Tutorial](https://lfereman.github.io/)

### Installation 

1. Clone the repository
2. Code is implemented in `Python` (3.9), but some performance-critical code is implemented in `Java` (1.8) . 
3. Precompiled Java code is provided. In case Java code is changed, run `run mvn clean install assembly:single` 

### Usage
**PBADN** is a library for finding similar time series and anomaly detection

Parameters for `preprocesssing` are (module `symbolic_representation`):

- `interval` and `stride` control the creation of  _fixed sized sliding windows_  in continous time series. Interval and stride are defined in hours
- `no_symbols` is used for reducing the dimensionality of a continuous timeseries using a  _moving average_  (i.e. Piecewise Aggregate Approximation).
- `no_bins` controls the number of bins for  _equal length discretisation_ .
- `preprocess_outlier` if `True` removes outliers based on the 0.99 and 0.01 quantile
 
Parameters for `run_ipbad` are (module `ipbad.main`):

- `use_MDL` if `True` then patterns are filtered based on  _minimum description length_
- `use_iso` if `True` an  _isolation forest_  is used for anomaly prediction, else the  _frequent pattern outlier factor_  (Fpof) is used
- Use `ipbad.main.run_ipbad` for running the pattern-based anomaly detection (standalone or for networks) with minimum description length post-processing and `run_ipbad_mv` for multivariate time series

Main algorithms for finding similar time series is:

- `ts_simil.fingerprinting` for finding most similar time series using  _fingerprinting_ (`find_similarities_fingerprinting`) and histogram distances

We illustrate both classes in the following example:

```
iimport matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from npbad.utils import load_numenta_data
from npbad.ipbad.main import run_ipbad
from npbad.eval_methods import eval_best_f1
from npbad.visualisation import plot_data

taxi = './data/realKnownCause/nyc_taxi.csv'
labels = './data/realKnownCause/combined_labels.json'
df, anomalies = load_numenta_data(taxi, labels)

#run anomaly detection
an_scores, ts_i, segments_discretised, windows = run_ipbad( df=df, interval=96, stride=1, no_symbols=5, no_bins=10)                                                                             
(f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies, point_adjust=False)
print(f"F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
#F1@best: 0.4636, precision: 0.4232, recall: 0.5124, threshold: 0.9994, TP: 248, FP: 338, FN:236, TN:4242

#plot time series, discrete sequence and groundtruth and predicted anomalies
fig, axs = plt.subplots(2, figsize=(30,3), sharex=True)
plot_data(axs[0], df, anomalies)
timestamps = [window[0] for window, score in an_scores]
scores = [score for window, score in an_scores]
axs[1].plot(timestamps, scores, color='red',alpha=0.5)
axs[1].axhline(y=t, color='b', linestyle='--')
axs[1].set_ylabel('IPBAD')
plt.show()
```

Implementation of state-of-the-art baseline algorithms are also provided: 

- `baselines.fpof.run_fpof` for running the frequent pattern outlier factor (FFOF) method. [Fp-outlier: Frequent pattern based outlier detection](https://www.researchgate.net/profile/Zengyou_He/publication/220117736_FP-outlier_Frequent_pattern_based_outlier_detection/links/53d9dec60cf2e38c63363c05/FP-outlier-Frequent-pattern-based-outlier-detection.pdf). 
- `baselines.pbad.run_ipbad` for running the patter-based anomaly detection (PBAD) method. [Pattern-based anomaly detection in mixed-Type time series](https://lirias.kuleuven.be/retrieve/550408).
- `baselines.iso_forest.run_iso_forest` and `run_iso_forest_mv` for running the isolation forest method (after transforming each time series using a sliding window)
- `ts_simil.paa` for finding most similar time series  _Piecewise Aggregate Approximation_  (PAA) (`find_similarities_paa`) 
- `ts_simil.pearson` for finding most similar time series using  _Pearson Correlation Coefficient_  after downsampling
- `ts_simil.show_network.py` Code for rendering network using `networkx` (a python package)

### More information for researchers and contributors ###
The main implementation is written in `Python`.  Performance-critical code, mainly for computing patterns and the embeding, is implemented in `Java`. For mining closed or maximal itemsets and sequential patterns for baselines (FPOF and PBAD) we depend on the `Java`-based [SPMF](www.philippe-fournier-viger.com/spmf/) library. Python Dependencies are `numpy`, `pandas`, `scikit-learn`, `scipy1`, `seaborn`, `tqdm`, `networkx`, `PIL` and `pyts`.
 
Datasets are provided in _/data_:

- `realKnownCause` *New york taxi*, *ambient temperature*, and *request latency*. Origin is the [Numenta repository](https://github.com/numenta).
- `multivariate` *SMD* dataset captured from a large Internet company. Origin is [OmnyAnonomaly github](https://github.com/NetManAIOps/OmniAnomaly).

To run experiments that compare **PBADN** with state-of-the-art methods see  `src/python_experiments`.

Supporting modules are:

- `anomaly_detection` for predictions using an isolation forest from `scikit-learn` or frequent pattern outlier factor 
- `symbolic_representation` Code for  _sliding window_ and  _discretisation_
- `eval_methods` To evaluate best F1 using point-adjust or recall@k/precision@k
- `visualisation` Code for  _visualisation_ of time series, symbolic representations and patterns
- `skyline` Code for loading proprietary data
- `preprocess_timeseries.py` Code for pre-processing time series, i.e. normalising, removing outliers, etc.
- `ipbad.huffman` Adjusted [open-source code](https://github.com/YCAyca/Data-Structures-and-Algorithms-with-Python/blob/main/Huffman_Encoding/huffman.py) from computing number of bits using huffman encoding
- `ipbad.pattern_mining_petsc` Wrapper for mining  _top-k sequential and cohesive patterns_  and generating the  _embedding_ (in `Java`), based on [PETSC: Pattern-based Embedding for Time Series Classification repo](https://bitbucket.org/len_feremans/petsc)
- `ipbad.minimum_description_length` Methods for compute  _minimal description length_  for patterns
- `baselines.pattern_mining_spfm` Method for calling the [SPMF](https://www.philippe-fournier-viger.com/spmf/) algorithms (used by pbad and fpof), code from [Pbad repo](https://bitbucket.org/len_feremans/pbad/).
- `python_experiments/` Different experiments reported for running experiments on univariate and multivariate benchmark datasets

### Contributors

- Len Feremans, Adrem Data Lab research group, University of Antwerp, Belgium.

### Licence ###
Copyright (c) 2022 Len Feremans

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFWARE.