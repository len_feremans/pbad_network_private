\section{Pattern-Based Anomaly Detection in a Network of Multivariate Time Series}
\label{alg}

%\subsection{Problem setting}
%The problem we are trying to solve can be defined as:
%\begin{description}
%	\item[Given:] A network of multivariate time series (devices) %A univariate (sensor), multivariate (device), or
%	\item[Do:] Identify periods of abnormal behaviour
%\end{description}

The problem we are tackling can be defined as follows:
Given a network of multivariate time series (or devices) identify periods of abnormal behaviour.

\begin{figure}[t!]
    \centering
    \includegraphics[width=5cm, trim=6.7cm 22.5cm 7cm 3.2cm, clip=True ]{plots/plots_SAX_representation_page.pdf}
    \caption{Illustration of the \textsc{Sax} transform where we transform a time series window with length $128$ into the discrete sequence $(b,a,a,b,c,c,b,c)$ of length $8$ using non-overlapping sliding window to compute average values ($\mathit{paa\_win}=16$) and binning those values ($\mathit{no\_bins}=3$)}
    \label{fig1}
\end{figure}

\subsection{An efficient algorithm to identify inter-device similarities}
The first step is to determine edges between devices by identifying similar time series on different devices, which is a hard problem. A na\"ive method would compare each pair of time series, however, the runtime would be $O(\frac{N^2}{2} \cdot |S|)$ where $N$ is the total number of time series in the network, which is not scalable just as it is inefficient to keep every time series in memory. Another issue is that when we compute $N^2/2$ candidates using common similarity measures we identify many \emph{false positives due to chance}. %Finally, we remark that the absolute value of common similarity measures, such as Euclidean distance, Pearson correlation and Dynamic Time Warping is not \emph{equitable} \cite{kinney2014equitability}. For instance, using a single threshold on the similarity some time series such as straight lines are similar to many other time series, while time series with more noise are similar to none.
In this section, we propose an efficient algorithm that: (i) represents each time series using a small \emph{fingerprint} having a size $k \ll |S|$, (ii) tests if the \emph{distribution} of time series is similar to avoid false positives, (iii) builds a network based on frequent relations between sensor types to avoid remaining false positives. The main \textsc{BuildNetwork} algorithm is shown in Algorithm~\ref{alg:search}.
%(ii) avoids uncompressing time series stored using \emph{run-length encoding} (RLE) in memory where $|\mathit{rle}(S)| \ll |S|$, 

\subsubsection{Pre-processing}
%\medskip
%\noindent\textbf{Pre-processing}
During pre-processing we load each time series one by one, cap outlier values based on the 99\% quantile and apply min-max normalisation. Additionally, we check and remove straight lines by fitting a linear function using least squares to the time series and checking if the Pearson correlation coefficient between the time series and the straight line is higher than a fixed threshold, i.e.,  0.98.

\subsubsection{Density-based fingerprint}
%\medskip
%\noindent\textbf{Density-based fingerprint}
Next, we propose to generate a \emph{density-based fingerprint}, or a low dimensional (visual) summary for each time series that is efficiently compared using Euclidean distance.
%In related work, authors have proposed using \textsc{Paa} or Product Quantization that both use a comparable strategy of reducing the dimension of each time series and then computing the full $N^2/2$ similarities thereby using GPU processing to scale to billions of time series~\cite{jegou2010product,johnson2019billion}. Also related is the maximal information coefficient where similarity is computed using mutual information on a discretised grid of values \cite{kinney2014equitability}.
We create a grid of $n \times k$ cells for each time series thereby computing the \emph{density}, i.e., the number of samples $x_i$ in each cell. Additionally, we use \emph{logarithmic scaling} to give relatively more weight to low-density cells such as peaks.  Formally, the distance between two time series using fingerprinting is defined as
$$\mathit{dist\_fp}(S^i,S^j,n,k) = \sum_{x=1}^{n}\sum_{y=1}^{k} \| \mathit{log}(I_{x,y}(S^i) + 1) - \mathit{log}(I_{x,y}(S^j) + 1) \|_2 $$
where $I_{x,y}(S)$ denotes the number of sensor values in the cell $(x,y)$. Note that in the limit, i.e., for high values of $n$ and $k$, the fingerprint distance converges towards the Euclidean distance. Because we are dealing with time series, we usually set $n$ using an \emph{interval} and discretise the sensor values into a small number of \emph{bins}. 
An example is shown in Figure \ref{fig2}. We highlighted the \emph{concept drift} in green which foils common distance measures, such as Pearson correlation and Dynamic Time Warping. In this example, the  distance between the two fingerprints is low as there is a large correspondence in \emph{peaks}, i.e., light grey cells, and the overall \emph{trend}, i.e., black and white cells.
 % where we show two time series instances that are not similar using either Pearson correlation or Dynamic Time Warping because of underlying concept drift, yet are similar when comparing the distance of their fingerprints because both the peaks and the overall trend are similar. 

 

\begin{figure}[b!]
    \centering
    \includegraphics[width=8.5cm, trim=1.5cm 2.5cm 2cm 3cm, clip=True ]{plots/Fingerprint.pdf}
    \caption{Example of $2$ continuous time series ($|S| = 28\ 224$ and duration is $103$ days). The corresponding density-based fingerprints are created by counting values in each cell created using an interval of $24$ hours and $4$ bins ($\mathit{fingerprint}(|S|) = n \times k = 103 \times 4$)} 
    \label{fig2}
\end{figure}

\input{section_3_method_algo_search}

\subsubsection{Histogram distances}
%\medskip
%\noindent\textbf{Histogram distances}
We remark that common similarity metrics such as Pearson correlation, \textsc{Paa} and fingerprinting often report time series as similar if the overall trend is similar. However, we argue that similarity in the overall trend is often due to chance as many time series exhibit a similar overall trend and are mostly flat, or monotonically increasing or showing a clear day/night fluctuation. The histogram is invariant to phase changes and complementary to distance measures in the time domain. Therefore, we propose histogram distances as a second criterion to avoid false positives. 
Given time series $S^i,S^j$ we define the distance between histograms as  
$$\mathit{dist\_hist}(S^i,S^j, l) = \sum_{k=1}^{l} |H^k(S^i) - H^k(S^j)|$$
where $l$ is the number of equal-length bins and $H^k(S)$ denotes the number of sensor values in bin $k$ divided by $|S|$. Finally, we define two time series are similar, i.e. $S^i\sim S^j$, if  $$\mathit{dist\_fp}(S^i,S^j,n,k)  < t_f \wedge  \mathit{dist\_hist}(S^i,S^j,l) < t_h$$
where $t_f$ is a threshold for fingerprint-based distance and $t_h$ is a threshold for histogram-based distance.
% We remark that more advanced techniques for comparing distributions of time series have been studied \cite{cha2002measuring}. 
The proposed distance measure has a linear complexity, i.e., $O(n \times k + l)$ and keeping $n\times k + l \ll |S|$ values in memory is efficient.  
In Algorithm \ref{alg:search} we load time series one by one and compute this memory-friendly representation (line \ref{line:startprep}-\ref{line:endprep}). Next, we compute the distance between all $N^2/2$ candidates and create a similarity matrix $\mathbf{M}$ (line~\ref{line:startsimil}-\ref{line:endsimil}).

\subsubsection{Build a network of devices}
%\medskip
%\noindent\textbf{Build network of devices} %based on frequent relations in sensor type}
When discovering and visualising a network of connected devices using $\mathbf{M}$, we noticed that there are clear patterns in the type of relations between entities, e.g., an Aperi video gateway often has the same CPU load as another Aperi video gateway, which is explainable since these devices process the same data stream in parallel. However, due to chance, we might find spurious similarities. For instance, we might discover a single pair of devices where the CPU temperature of an Aperi video gateway device is similar to the network bit rate due to chance. 
%
Motivated by this observation we post-process spurious relations in the similarity matrix $\mathbf{M}$, which we consider to be an \emph{attributed network}, i.e., $E(G) = \{(S^k,S^l) \ | \ S^k \sim S^l\}$. We define the \emph{support} of different types of edges between devices in this attributed network using
\begin{equation}
\begin{gathered}
\mathit{support_{RT}}(G, \mathit{type_i}, \mathit{type}_j) = \\
|\{(D_x,D_y) \ | \exists S^k \in D_x, S^l \in D_y:  \\
(S^k, S^l) \in E(G) \wedge \mathit{type(S^k)} = \mathit{type}_i \wedge \mathit{type(S^l)} = \mathit{type}_j\}|.
\end{gathered}
\end{equation}
We discover the most frequent relation types thereby providing a \emph{comprehensible summary} of the edges in the network. %Using the proposed method, we find that $k$ is often much smaller than the maximum number of theoretically possible pairs of types.
In Algorithm~\ref{alg:findfrequent} we show the algorithm for discovering frequent relation types based on \emph{coverage}. First, we join all pairs of discovered similar time series with metadata, i.e., the device and type of both time series. Next, we identify the relation type between sensors with the highest support (line \ref{line:toprel}). %For instance, there might be 100 Aperi video gateway devices connected by a pair of similar CPU load sensors. 
\begin{algorithm2e}[b!]
\SetKwInOut{Input}{Input}
\caption{\textsc{FindFrequentRelationTypes}($\mathbf{M}$, $\mathit{coverage}$) Find frequent relation types based on coverage}
\Input{A similarity matrix $\mathbf{M}$ between time series; $\mathit{coverage}$ (\%) parameter, i.e., percentage of non-zero relations in $\mathbf{M}$ that have to be matched by a frequent relation type}
\KwResult{Set of frequent relation types between devices}
%\BlankLine
\label{alg:findfrequent}
   \LinesNumbered
            $\mathbf{T} \gets \emptyset$\\
             \label{line:startpp}
            \ForEach{$ (i,j) \in \mathbf{M}$}{
                \If{$\mathbf{M}_{i,j} > 0 \wedge \mathit{device}(S^i) \neq \mathit{device}(S^j)$}{
                 $\mathbf{T} \gets \mathbf{T} \cup (\mathit{device}(S^i), \mathit{device}(S^j), \mathit{type}(S^i),  \mathit{type}(S^j))$}
            }
            $\mathit{freq\_relations} \gets \emptyset$\\
            $\mathit{min\_cover} \gets |\mathbf{T}| * (1 - \mathit{coverage})$\\
            \While{$|\mathbf{T}| > \mathit{min\_cover}$}{
                %\mathit{freq\_rel} \gets \underset{(t_1,t_2) \in \pi_{\mathit{type}_1,\mathit{type}_2}(\mathbf{T})} %{\mathit{argmax}}|\{ (r.dev_1, r.dev_2) \ | \  r \in \mathbf{T}: (r.type_1 = t_1 \wedge r.type_2 = t_2) \lor %(r.type_1 = t_2 \wedge r.type_2 = t_1)\}|$\\
                $\mathit{freq\_rel} \gets \underset{(t_1,t_2) \in \pi_{\mathit{type}_1,\mathit{type}_2}(\mathbf{T})} {\mathit{argmax}} \mathit{support_{RT}}(T, t_1,t_2)$\\
                \label{line:toprel}
                $\mathit{freq\_relations} \gets \mathit{freq\_relations} \cup \mathit{freq\_rel}$\\ 
                $\mathit{device\_pairs} \gets \pi_{dev_1,dev_2} ( \sigma_{\mathit{type}_1 = t_1 \wedge \mathit{type}_2 = t_2}(\mathbf{T}))$\\
                $\mathbf{T} \gets \mathbf{T} \ \setminus \ \{ r \ | \  r \in \mathbf{T}: (r.dev1,r.dev2) \in \mathit{device\_pairs}\}$
            }
            $\mathbf{return} \text{ } \mathit{freq\_relations}$\
\end{algorithm2e}
Next, we identify all pairs of devices connected through this frequent relation type and remove all edges between these devices. We keep on mining the next most frequent relation types recursively until we cover a sufficient number of the discovered edges between time series as determined by the $\mathit{coverage}$ hyper-parameter. We remark that a pair of devices is often connected based on multiple correlated time series, however by removing pairs of devices at each iteration we focus on \emph{non-redundant} relation types by computing the support of a relation type after filtering the device pairs connected through higher support relation types. Finally, we \emph{build the network} of connected devices based on the discovered frequent relations in the similarity matrix as shown in Algorithm \ref{alg:search} (line \ref{line:endpp}).  %we \emph{remove any remaining relations between time series} that are infrequent and therefore likely spurious. 

