

\subsection{Pattern-based anomaly detection in univariate time series based on minimum description length}
For anomaly detection in univariate time series, we first transform each time series into a sequence database and discover a set of sequential patterns. Next, we create an embedding where each window is represented by pattern occurrences. Finally, we compute an anomaly score for each window. The main algorithm \textsc{IPbad} is shown in Algorithm \ref{alg:ipbad}.
%TODO: LEN inkorten

\subsubsection{Discovering sequential patterns}
%\medskip
%\noindent\textbf{Discovering patterns} 
%In theory, given a sequence database $\mathcal{D}$ where the sequence length is $d$ and there are $n$ bins there are n^{d+1}$ possible candidate sequential patterns. 
%However, most 
Algorithms for discovering patterns leverage the \emph{anti-monotonicity} of support whereby longer patterns always have a lower support than their (prefix) subsequences, i.e., $\forall X_s \sqsubseteq \mathit{X}: \mathit{support}(X_s) \ge \mathit{support}(X)$~\cite{zaki2014data}. For mining patterns in time series, we adopt an existing algorithm that discovers frequent sequential patterns subject to temporal constraints \cite{feremans2022}. The algorithm mines the \emph{top-$k$ frequent patterns directly}, where we typically set $k$ to at most $10\ 000$, as fine-tuning the threshold on \emph{minimal support} to get a sufficient number of patterns is resource consuming. A  problem with frequent pattern mining is that many patterns are redundant, which is intensified by the fact that time series are typically auto-correlated. We use a constraint on \emph{minimum length} to avoid  short sequential patterns and on \emph{relative duration}, or cohesion, to ensure that any occurrence of $X$ in $\hat{S}_{i:j}$ contains at most $|X| \times (\mathit{rel\_dur} - 1)$ gaps.  For instance, with a relative duration of $1.0$ all pattern symbols must occur consecutively in $\hat{S}_{i:j}$. %Baseline algorithm does not put a limit on the number of gaps between items in a pattern occurrence, i.e., $X=(a,b,c)$ also matches the sequence $(a,b,x,x,x,x,c)$ which foils accuracy when classifying time series \cite{feremans2022}. 


%To further limit the number of candidates we rely on \emph{prefix-projected pattern growth} \cite{pei2004mining}.

%, which is memory-friendly given the depth-first search strategy of searching for frequent patterns. 
%A first problem with baseline sequential pattern mining algorithms, such as PrefixSpan \cite{pei2004mining}, is that fine-tuning the threshold on \emph{minimal support} is resource consuming, where a too small threshold results in millions of patterns. Instead we propose to use an algorithm that mines the \emph{top-$k$ frequent patterns directly}, where we typically set $k$ to at most $10\ 000$. 

\subsubsection{Filtering sequential patterns using minimal description length}
%\medskip
%\noindent\textbf{Filtering sequential patterns using minimal description length} 
A problem with the above algorithm for mining sequential patterns is that, while a sequential pattern and its occurrences can be inspected if an anomaly is reported, this is arguably \emph{less comprehensible} if there are thousands of patterns. 
%A second problem is that we introduced \emph{many hyper-parameters}, i.e., three parameters for pattern mining ($k$, $\mathit{min\_len}$ and $\mathit{rel\_dur}$) and two parameters for transformation to discrete sequences using SAX ($\mathit{paa\_win}$ and $\mathit{no\_bins}$).
Motivated by this problem, we post-process sequential patterns and filter patterns based on the \emph{Minimum Description Length} principle \cite{grunwald2007minimum, shokoohi2015discovery}. That is, we only keep a frequent pattern describing a regularity in the time series if the pattern compresses the data, i.e., if we need fewer symbols to encode the data using the pattern (including the pattern itself) than we need to encode the data literally.
Formally, given a pattern $X$ we define the number of bits saved using
\begin{equation}
    \begin{gathered}
    \mathit{bits\_saved}(X) = - |X| \cdot log(n) \ + \\
    \qquad \qquad \qquad \sum_{\hat{S}_{i:j} \in \mathit{cover}({X,\mathcal{D})}}{\mathit{DL}(\hat{S}_{i:j}) - \mathit{DL}(\hat{S}_{i:j}| \ X)}.
    \end{gathered}
\end{equation}
Here $\mathit{DL}(\hat{S}_{i:j})$ is the description length of a sequence, i.e., the number of bits to store the sequence using Huffman encoding. $|X| \cdot log(n)$ is the number of bits used to store the pattern, and $\mathit{DL}(\hat{S}_{i:j} | X)$ is computed by replacing the pattern occurrence of $X$ in $\hat{S}_{i:j}$ and then computing the description length of the remaining symbols. For example, assuming $X=(a,b,c)$ and $\hat{S}_{i:j}=(x,y,a,b,c,z)$ we compute the number of bits used to store  $(x,y,z)$. We remark that $\mathit{bits\_saved}$ is maximal for patterns that are both long and frequent, while frequency by itself is always maximised by shorter patterns. Finally, we only keep patterns where $\mathit{bits\_saved}(X) > 0$ which often results in a dramatic reduction of the number of patterns. % (e.g., fewer than 100 patterns given an original $k$ of $10\ 000$).
%The proposed algorithm mines all frequent cohesive sequential patterns with default parameters, i.e. $k=10\ 000$, $\mathit{min\_len}=3$ and $\mathit{rel\_dur}=1.0$. During post-processing we filter all patterns where $\mathit{bits\_saved}(X) > 0$ which often results in a dramatic reduction of the number of patterns, i.e., fewer than 100 patterns given $k=10\ 000$.

\subsubsection{Creating a pattern-based embedding}
%\medskip
%\noindent\textbf{Creating a pattern-based embedding} 
Given a set of patterns $\mathcal{P}=\{X_1,X_2\ldots,X_k\}$ we construct an embedding $E_{i:j}$ for each discretised window $\hat{S}_{i:j}$, using
$$E_{i:j}=(f(X_1,\hat{S}_{i:j}), \ldots,  f(X_k,\hat{S}_{i:j})) \quad \text{and}$$
$$f(X_n,\hat{S}_{i:j})= \left \{ 
  \begin{array}{ c l }
    \mathit{rsupport}(X_n,\mathcal{D})  & \quad \textrm{if } X_n \prec \hat{S}_{i:j} \\
    0                 & \quad \textrm{otherwise}
  \end{array},
$$
where we use the \emph{relative support} of a pattern as a feature as opposed to a simpler binary function i.e., 1 if $X_k \prec \hat{S}_{i:j}$ and 0 otherwise or a distance-weighted similarity score \cite{feremans2019pattern}. 

A na\"ive algorithm for computing the embedding would take $O(|\mathcal{P}| \times |\mathcal{D}|)$ steps. However, we create an embedding more efficiently by leveraging the \emph{sparsity} of the embedding. In Algorithm \ref{alg:ipbad} (line \ref{line:startemb}-\ref{line:endemb}) we use prefix-projected pattern growth to identify all segments matching each pattern $X_n$ without the cost of matching the full pattern $X_n$ to all sequences in $\mathcal{D}$~\cite{han2001prefixspan}.

\subsubsection{Computing the anomaly score}
%\medskip
%\noindent\textbf{Computing the anomaly score using an isolation forest}
Finally, we compute anomaly scores using an \emph{isolation forest} \citep{liu2008isolation}. Given an embedding $E_{i:j}$ we compute a low anomaly score if many frequent patterns occur and the (average) depth of random trees is relatively high, or conversely a high anomaly score if few frequent patterns occur in the embedding vector for each window.  %This is in contrast to FPOF where the anomaly score is based on the ratio of the number of matching patterns versus the total number of patterns.

\input{section_3_method_algo_pbad}

\subsection{Pattern-based anomaly detection in a network of multivariate time series}
%TODO: Algo MAIN... 


Given the previously defined %efficient 
algorithms that identify connected devices and contextual anomalies we are now able to tackle the main problem of detecting anomalies at the entity and network level. 

%\subsubsection{Sensor anomaly where the context is a single time series}
%Traditionally, methods take a single time series as input and compute an anomaly score for each timestamp. If we create a sequence database and run \textsc{IPbad} we have a simple method that computes anomalies and is parameter free. 


%\subsubsection{Sensor anomaly where the context is a cluster of similar time series}
%However, in our telecom dataset we have different entities of the same type and each entity has the same set of sensors connected in a network. Therefore, we can rephrase the question: ``is the CPU Load of the Arista MPEG device 1 abnormal based on past behaviour?" into ``is the CPU Load of Arista MPEG device 1 abnormal based on the behaviour of that sensor and other sensors of the same type?". 

%We propose to create a sequence database $\mathcal{D}_{C}$ of all time series in one \emph{homogeneous cluster}. A cluster can be defined using \emph{type}, e.g., all ``Arista MPEG CPU load time series" or based on \emph{connected} devices. e.g., the ``Arista MPEG CPU load of device 1 and connected Arista MPEG devices". For discovering normal and frequent behaviour in a homogeneous cluster of time series $C$ we first create a single sequence database for each time series $S^i \in C$, i.e. $$\mathcal{D}_{C} = \matit{concatenate}(\{\mathcal{D}_{S^i} \ | \ S^i \in C\}).$$
%Next, we discover a set of frequent, cohesive and compressing patterns $\mathcal{P}_C$ in $\mathcal{D}_{C}$ and create an  embedding $\mathbf{E}^i$ for each time series $S^i$ using an isolation forest to compute anomalies as before. Alternatively we compute the anomaly score of all time series in $C$ at the same time and only report anomalous periods for time series that have a high anomaly score relative to anomaly scores within the cluster.

%\subsubsection{The odd time series in a cluster of time series}
%Alternatively we compute the anomaly score of all time series in $C$ at the same time and report anomalous periods in time series that have a high anomaly score in the cluster. In this task, we mine the pattern set $\mathcal{P}_C$ as before and also concatenate the embedding:
%where we compute the anomaly score using a single isolation forest trained on $E_C$ and do trivial post-processing to map back from $E_C$ to $S^i_{i:j}$. We argue this task is useful towards end-users because it is more manageable to inspect the top-scores of a subset of time series, than each time series, especially if $C$ is large and anomalies are rare. 

%\subsubsection{Entity anomaly where the context is a multivariate time series}
\subsubsection{Entity anomalies in multivariate time series}
%\medskip
%\noindent\textbf{Entity anomalies in multivariate time series} 
For entity-level anomaly detection we discover patterns in each time series \emph{independently} as time series are of different types and an entity $D$ is in essence a \emph{heterogeneous} collection. %Entity anomalies in multivariate time series and create a pattern-based embedding $E^k_{i:j}$ for eac%h window in each of the $m$ time series $S^k \in D$. For computin
Given an entity $D=\{S^1,\ldots,S^m\}$ we concatenate the discovered embedding vectors in each time series $S^k$ for each window using
$$E^{1,\ldots,m}_{i:j} = \mathit{concatenate}(\{E^k_{i:j} \ | \ S^k \in D\}).$$
%$$E^{1,\ldots,m}_{i:j} = \mathit{concatenate}(\{E^k_{i:j} \ | \ S^k \in D\}).$$
Next, we use an isolation forest thereby predicting a window $D_{i:j}$ as anomalous if one or more sensors behave unexpectedly, i.e., if the joint occurrence of patterns is unusual. The advantage of an isolation forest is that dependencies between patterns are captured, i.e., if $X_1 \in \mathcal{P}^1$ and $X_2 \in \mathcal{P}^2$ always occur together, then an anomaly would be flagged if only $X_1$ occurs, which is especially important when combining patterns in multivariate time series. We remark that we could also discover frequent patterns in multivariate sequences directly by creating a single sequence of itemsets with discrete items from each of the $m$ dimensions. However, this representation is likely to cause pattern explosion.% especially with larger values of $m$. %, i.e. given a sequence $\hat{S}^1=(a^1,a^1,a^1,b^1)$ and $\hat{S}^2=(b^2,b^2,b^2,c^2)$ we create a single sequence of itemsets over $m$ dimensions $\hat{S}^{1,2}= (\{a^1,b^2\}, \{a^1,b^2\}, \{a^1,b^2\}, \{b^1,c^2\})$ in order to discover sequential patterns with items from multiple dimensions. 

%\subsubsection{Entity anomaly where the context is a multivariate time series and connected entities}
\subsubsection{Entity anomalies in a network of connected entities}
%\medskip
%\noindent\textbf{Entity anomalies in a network of connected entities} 
Finally, we  tackle the most difficult case where we consider all sensors of a device and connected devices. In this case, we create a single collection $D_{\mathit{ext}}$ where we add all time series of the device and similar time series of connected devices, that is 
\begin{equation*}
    \begin{gathered}
    D_{\mathit{ext}}= D \cup \{S^j \ | \  \exists \ S^i \in D, \ S^j \in D_k: (D,D_k) \in E(G) \wedge  S^i \sim S^j\}.
    \end{gathered}
\end{equation*}

Next, we mine patterns and create an embedding for each time series independently, and concatenate the embedding vectors as in the previous case. For instance, if $S^i$ and $S^j$ are similar (e.g. the sending and receiving bit rates are similar) we expect that a leaf of a tree in an isolation forest will often match with many instances where frequent patterns of $\mathcal{P}^i$ and $\mathcal{P}^j$ co-occur causing a high anomaly score if this is not the case. 
%
%\subsubsection{Comprehensive entity anomaly detection}

\subsubsection{Interpretable anomaly scoring}
%\medskip
%\noindent\textbf{Interpretable anomaly scoring}
To improve interpretability we compute anomalies based on the \emph{frequent pattern outlier factor} (\textsc{Fpof})~\cite{he2005fp} instead of using an isolation forest and compute the anomaly score \emph{independently} for each time series in $D_{\mathit{ext}}$. Ignoring interactions between patterns in different time series can result in a lower accuracy. However, the anomaly score can be inspected on the level of the individual time series (or connected time series). For instance, if at window $D_{i:j}$ only time series $S^k,S^n$ and $S^l$ have a high anomaly score while others are normal this \emph{localisation} information is useful to the end-user \cite{su2019robust}. The anomaly score in a single time series window $S_{i:j}$ is computed using $$\mathit{fpof}(\hat{S}_{i:j}) = \frac{1}{|\mathcal{P}|} \sum_{X \in \mathcal{P} \wedge X \prec \hat{S}_{i:j}} \mathit{rsupport}(X, \mathcal{D}).$$ 
Here, the outlier factor is high (close to 1) if many frequent patterns (weighted by the relative support) match the current window and low otherwise. For ranking anomaly scores we use $1 -\mathit{fpof}(\hat{S}_{i,j})$.