\section{Experiments}
\label{exp}

In this section, we present the results of our experimental evaluation. We begin by evaluating our method for identifying inter-device similarities, before comparing our anomaly detection method to state-of-the-art algorithms on several benchmark datasets.

\subsection{Identifying inter-device similarities}

%\noindent\textbf{Identifying inter-device similarities} 
We first ran \textsc{BuildNetwork}\footnote{The open-source implementation is available at ANONYMIZED URL} on the broadcasting video network dataset.  %\url{https://bitbucket.org/len_feremans/pbad\_network/}.
Each sensor was collected over 98 days where the mean value every 5 minutes is stored resulting in $28\,224$ values. For fingerprinting, we set the interval to 24 hours and use 5 bins for discretisation. For the histogram creation, we use 100 equal-length bins. We set the threshold for fingerprinting using the 99\% quantile and for histogram distances using the 95\% quantile. The coverage parameter is set to 95\%. 

\subsubsection{Comprehension}
%\medskip
%\noindent\emph{Comprehension} 
We find $9\,458$ pairs of similar time series using the supplied threshold. We exclude intra-device similarities resulting in a reduction of about $49.1\%$. To simplify the analysis we remove edges between known groups of devices, resulting in a further reduction of about $82.7\%$.  Finally, using coverage results in a reduction of $61.5\%$ of edges resulting in only $351$ edges between devices. 
In Table \ref{tab:frequenttypes} we show the top-6 of the total $28$ frequent relation types which is much smaller than the $\binom{49}{2} = 1176$ theoretically possible combinations. These frequent relations are a good starting point to \emph{comprehend} this complex network of heterogeneous devices and sensors. 

\begin{table*}[h!]
%\resizebox{\textwidth}{!}{%
\begin{tabularx}{0.8\textwidth}{l *{4}{Y}}
\toprule
\textbf{Device type 1} & \textbf{Device type 2} &  \textbf{Relation type} & \textbf{No edges}\\
\toprule
Aperi video gateway & Aperi video gateway & System load & 91\\
Aperi video gateway  & Aperi video gateway  & CPU load & 52\\
Aperi video gateway  & Aperi video gateway  &  IP bit rate & 36\\
Aperi video gateway & Aperi video gateway  &  TS rate & 29\\
Aperi video gateway  & Aperi video gateway &  User load & 24\\
Aperi chassis  & Aperi video gateway   &  \makecell{CPU system time-\\CPU system load} & 10\\
 \bottomrule
\end{tabularx}%
%}
\caption{Top-$6$ most frequent relation types between devices
}
\label{tab:frequenttypes}
\end{table*}

\subsubsection{Efficiency and accuracy}
%\medskip
%\noindent\textbf{Efficiency and accuracy} 
Computing the similarity of all $78.8\cdot10^6$ possible pairs \emph{takes just $8.5$} minutes or $59.0$ ms per instance to search in $8\,708$ time series after excluding straight lines. In Figure~\ref{fig:network1} we show the resulting network visualised using the NetworkX  package. We find 4 large dense clusters of mostly Aperi video gateway devices which are a type of card located inside the same Aperi Chassis device, so, unsurprisingly, they have multiple similar sensors and a connection with a few Aperi Chassis elements. We also discover interesting smaller clusters, e.g., Cisco DCM, and Aperi Chassis devices are often connected using bit rate. Finally, we observe that most discovered relations are between devices where the identifier is near, e.g., device 1268 is connected with 1269. This validates that the proposed algorithm for identifying edges using only time series data matches with the prior knowledge that devices with a near human-assigned identifier are often related.

\begin{table*}[h!]
%\resizebox{\textwidth}{!}{%
\begin{tabularx}{0.8\textwidth}{l *{4}{Y}}
\toprule
\textbf{Method} & \textbf{Precision} & \textbf{Recall} & \textbf{F1}\\
\midrule
Fingerprinting and histogram & $\mathbf{0.971}$ & $0.779$ & $\mathbf{0.864}$\\ 
Piecewise aggregate approximation & $0.706$ & $\mathbf{0.895}$ & $0.789$\\
Pearson correlation coefficient & $0.462$ & $0.639$ & $0.536$\\
\bottomrule
\end{tabularx}%
%}
\caption{Accuracy of time series similarity identification in the broadcasting video network dataset}% The proposed approach has a higher precision and F1 but lower recall than \textsc{Paa}. Recall and precision for Pearson correlation coefficient are low.
%}
\label{tab:comparesim}
 \end{table*}
 
\begin{figure}[t!]
    \centering
    \includegraphics[width=8.5cm, trim=1.8cm 0.8cm 1.7cm 0.8cm, clip=True]{plots/final1.pdf}
    \caption{Discovered network of broadcasting video devices. The colour of each node represents a known cluster and the shape the type of device}
    \label{fig:network1}
\end{figure}
 
%\begin{figure*}[h!]
%    \centering
%    \includegraphics[width=11cm, trim=1.5cm 0.5cm 1.5cm 0.5cm, clip=True ]{plots/final1.pdf}
%    \caption{Discovered network of broadcasting video devices. The colour of each node represents a known cluster and the shape the type of device}
%    \label{fig:network1}
%\end{figure*}

\subsubsection{Comparing the accuracy of similarity measures}
%\medskip
%\noindent\textbf{Comparing the accuracy of similarity measures} 
We compare the proposed fingerprint-histogram distances against \textsc{Paa} \cite{keogh2001dimensionality} and the Pearson correlation coefficient after sampling each time series to 2000 values. %We remark that similarity is hard to judge, i.e., when assessing whether time series are similar from a human perspective we consider \emph{outliers}, i.e., two time series might have the same trend but peaks are different, \emph{entropy}, i.e., a flat trend seems more down to chance than a clear random trend or \emph{concept drift}. 
 To evaluate performance we compute
 \begin{equation*}
     \begin{gathered}
      F1=2\cdot\frac{\mathit{precision}\cdot\mathit{recall}}{\mathit{precision} + \mathit{recall}} \quad \text{ where }\\ \mathit{precision}=\frac{\mathit{TP}}{\mathit{TP}+\mathit{FP}} \quad \text{ and } \quad  \mathit{recall}=\frac{\mathit{TP}}{\mathit{TP}+\mathit{FN}}
     \end{gathered}
 \end{equation*}
 where $\mathit{TP},\mathit{FP}$ and $\mathit{FN}$ denote the true positives, false positives and false negatives. Since we do not have labels of similar time series, we pick 50 time series, search for the top-5 most similar time series for each of them, and manually assign labels based on domain knowledge. The results are shown in Table \ref{tab:comparesim}. We find that fingerprinting combined with histogram distances has a \emph{higher precision} and \emph{F1} but \emph{lower recall} compared to \textsc{Paa} and that the performance of Pearson correlation is relatively low. We remark that no single similarity measurement is a winner in all examples and types of time series. However, we argue that for context-aware anomaly detection high precision is of more importance than high recall.  


\subsection{Experiments on benchmark datasets}

%\paragraph{State-of-the art methods.}
%\medskip
%\noindent\textbf{Experiments on benchmark datasets} 
Since we have no ground truth for our network dataset, we perform additional experiments on univariate and multivariate benchmark datasets to compare our algorithm to existing methods and note that that none of these methods are designed for anomaly detection in a network of devices.   We compare \textsc{IPbad} with three state-of-the-art anomaly detection methods: \emph{Isolation forest} uses a forest trained on the original sensor values in each window~\cite{liu2008isolation}, \emph{Frequent pattern based outlier factor} (\textsc{Fpof}) computes an outlier factor based on the number of frequent closed itemsets~\cite{he2005fp}, and \emph{Pattern-based anomaly detection} (\textsc{Pbad}) uses an isolation forest trained on the embedding of raw sensor values, closed frequent itemsets and sequential patterns~\cite{feremans2019pattern}. Additionally, we compare with \textsc{IPbad-Fpof} which uses the frequent pattern based outlier factor instead of an isolation forest to facilitate interpretability. 

%\begin{itemize}
%\setlength\itemsep{0ex}
%    \item \emph{Isolation forest} uses a forest trained on the original sensor values in each window \cite{liu2008isolation}
%    \item \emph{Frequent pattern based outlier factor} (\textsc{Fpof}) computes an outlier factor based on the number of frequent closed itemsets~\cite{he2005fp}.
%    \item \emph{Pattern-based anomaly detection} (\textsc{Pbad}) uses an isolation forest trained on embedding of raw sensor values, closed frequent itemsets and sequential patterns \cite{feremans2019pattern}. We use a variant where we create an embedding based on the relative support instead of weighting using the distance.
%\end{itemize}

\subsubsection{Evaluation protocol}
%\medskip
%\noindent\emph{Evaluation protocol} 
Given a time series $S$ with a number of labelled timestamps, we divide the time series into fixed-sized sliding windows and compute an anomaly score for each window. Similar to the evaluation protocol of  \textsc{OmnyAnomaly}~\cite{su2019robust}, we adopt the \emph{point-adjust} approach where we consider an anomaly detected if it occurs within the boundary of a sliding window. Each method requires the setting of a threshold and we assume an Oracle that reports the best possible F1 value by varying the threshold. Since the isolation forest is not deterministic we run each method 5 times using the same parameters and report the mean values. 

\subsubsection{Parametrization}
%\medskip
%\noindent\emph{Parametrization} 
Each method has the same preprocessing steps which include setting the window size $l$ and increment $t$ to create sliding windows and parameters for transforming to a symbolic representation such as the \emph{paa\_window} for reducing the dimensionality and the number of equal-length \emph{bins} which we optimise using \emph{grid search}. For the \textsc{IsolationForest}, we use 500 \emph{trees}. For both \textsc{IPbad} and \textsc{Pbad}, we set $\mathit{min\_len}$ to 2. For \textsc{Pbad} and \textsc{Fpof}, we set the \emph{minimal support} to $0.01$. For \textsc{IPbad}, we set the number of patterns $k$ to $10\,000$ and the \emph{relative duration} to $1.2$.

\subsubsection{Datasets}
%\medskip
%\noindent\emph{Datasets}
For the univariate test case, we use 7 datasets. 
\texttt{Temp} tracks the ambient temperature \cite{lavin2015evaluating}.
\texttt{Latency} monitors CPU usage in a data centre \cite{lavin2015evaluating}.
\texttt{Taxi} logs the number of NYC taxi passengers~\cite{lavin2015evaluating}.
\texttt{Water} 1 to 4  is a proprietary dataset that contains the average consumption of water in 4 branches of a retail company where the goal is to detect abnormal water consumption~\cite{feremans2019pattern}. The water consumption is logged every 5 minutes for two years resulting in $170\,000$ values while the first 3 datasets contain at most $10\,000$ values. For the multivariate test case, we use the Server Machine Dataset (\texttt{SMD}) which consists of 28 entities, each consisting of 38 sensors containing $56\,960$ values~\cite{su2019robust}. The semantics are similar to our broadcasting video network with sensors such as CPU load, RAM usage, etc. Soil Moisture Active Passive (\texttt{SMAP)} is a real-world public dataset from NASA and consists of 54 entities each monitored by 25 metrics \citep{hundman2018detecting}.

\subsubsection{Results and discussion}
%\medskip
%\noindent\emph{Results and discussion}
Table~\ref{table:results_univariate} shows the F1 obtained by each method on each dataset. Results in bold highlight the best performing method on each dataset. The F1 score of \textsc{Fpof} is worse. We find that \textsc{IPbad} has the lowest average rank on univariate datasets thus outperforming existing state-of-the-art methods such as \textsc{IsoForest}, \textsc{Pbad} and \textsc{Fpof}. We find that both \textsc{IPbad} and \textsc{IPbad-Fpof} perform better than \textsc{IsoForest} on the multivariate datasets.  On the \texttt{SMD} dataset, we find the average F1 value of \textsc{IPbad} is $0.966$ $(\pm 0.002)$ which is higher than recent deep-learning based methods such as \textsc{OmniAnomaly} (0.962) and \textsc{Usad} (0.946)~\cite{audibert2020usad}. However, the F1 value for \texttt{SMAP} is lower, i.e. $0.775$ for \textsc{IPbad}  versus $0.853$ for \textsc{OmnyAnomaly} and $0.863$ for \textsc{Usad}. The relatively worse performance on \texttt{SMAP} is possibly explained by the special nature of this dataset, where time series often consist of only peaks alternating between two values. We report no results for \textsc{Fpof} and \textsc{Pbad} on the multivariate datasets as the run times are prohibitively long.

\begin{table*}[!h]
\begin{tabularx}{0.85\textwidth}{l *{6}{Y}}
\toprule
\textbf{Dataset} & \textsc{IsoForest} & \textsc{Fpof} & \textsc{Pbad} & \textsc{IPbad} & \textsc{IPbad-Fpof}\\
\midrule
\texttt{Temp} & 0.927 & 0.875 & 0.776 & \textbf{0.948} & 0.881\\
\texttt{Taxi} & 0.782 & 0.718 & 0.807 & \textbf{0.851} & 0.630\\
\texttt{Latency} & 0.827 & 0.653 & 0.864 & \textbf{0.901} & 0.845\\
\texttt{Water 1} & 0.686 & 0.333 & 0.755 & \textbf{0.787} & 0.549\\
\texttt{Water 2} & 0.528 & 0.450 & \textbf{0.596} & 0.539 & 0.452  \\
\texttt{Water 3} & \textbf{0.791} & 0.321 &  0.615 & 0.567 & 0.447\\
\texttt{Water 4} & 0.561 & 0.661 & \textbf{0.760} & 0.666 & 0.663\\
\emph{Avg. rank univariate} &  3 & 4.4 & 2.1 & \textbf{1.6} & 3.8\\
%Iso:           2 + 3 + 4 + 3 + 3 + 1 + 5 = 21 / 7 = 3
%FPOF:          4 + 4 + 5 + 5 + 5 + 5 + 4 = 31 / 7 = 4.4
%PBAD:          5 + 2 + 2 + 2 + 1 + 2 + 1 = 15 / 7 = 2.1
%IPBAD:         1 + 1 + 1 + 1 + 2 + 3 + 2 = 11 / 7 = 1.6
%IPBAD-FFpoF:   3 + 5 + 3 + 4 + 4 + 4 + 3  = 26 /7 = 3.8
\midrule
\texttt{SMD} & 0.949 & n/a & n/a & \textbf{0.966} & 0.957\\
\texttt{SMAP} & 0.708 & n/a & n/a & \textbf{0.775} & 0.733\\
\emph{Avg. rank multivariate} &  3 &  n/a &  n/a & \textbf{1} & 2\\
\bottomrule
\end{tabularx}
\caption{F1 score of various methods on $7$ univariate datasets and $2$ multivariate datasets consisting of $28$ and $54$ entities respectively, %(and 38 time series for each entity) 
where we report the mean result
}
\label{table:results_univariate}
\end{table*}

\subsubsection{Explaining anomaly scores} For decision support, it is of importance to explain not only when a device behaves anomalously, but also which sensor(s) caused the high anomaly score and which patterns (or their absence) contributed to this score. Therefore, we use \textsc{Ipbad-Fpof} to allow \emph{localisation} by computing the \textsc{Fpof}-based anomaly score based on a small number of patterns discovered in each time series. In Figure~\ref{fig:example1} we show 4 time series from the first device in the \texttt{SMD} dataset. For \texttt{SMD} we have labelled anomalies specific to a subset of time series highlighted in red. Below each time series we show the discrete representation computed using an interval of 6 hours, 10 symbols and 10 equal-length bins and the \textsc{Fpof} score in orange. In Figure \ref{fig:example2} we visualise the pattern-based embedding of the first sensor where the \textsc{Fpof} score is based on only 6 patterns. In this case, we find that after discretisation we have an auto-correlated discrete sequence that is mostly flat with first a series of 2s, then a series of 3s and then a series of 2s and that the set of patterns nicely summarises this. The known anomaly is located after the first series of $X_1=[2,2,2,\ldots]$ occurrences and before occurrences of $X_6=[3,3,3,\ldots]$, which is also when few patterns occur causing a high anomaly score.


\begin{figure*}[h!]
    \centering
    \includegraphics[width=13cm, trim=0cm 0cm 0cm 0cm, clip=True ]{plots/plot1_new.eps}
    \caption{Example of localised anomaly scores of 4 time series from a single device in the \texttt{SMD} dataset using \textsc{Ipbad-Fpof} . We show the time series before and after discretisation, the known sensor-specific anomalies (in red), and the predicted anomalies (in orange)}
    \label{fig:example1}
\end{figure*}


\begin{figure*}[h!]
    \centering
    \includegraphics[width=13cm, trim=0cm 0cm 0cm 0cm, clip=True ]{plots/plot-metric-7-patterns_new.eps}
    \caption{Example of a discrete representation of metric-7 from the \texttt{SMD} dataset where we show the pattern-based embedding that consists of occurrences of the discovered varying-length frequent sequential patterns in each window. We remark that after filtering w.r.t. constraints and minimal description length we have only 6 patterns}
    \label{fig:example2}
\end{figure*}


%water1=aalst, water2=hasselt, water3=Aarschot: tODO, water4=Heverlee 

\subsubsection{Runtime performance}
%\medskip
%\noindent\emph{Runtime performance}
Concerning \emph{runtime performance} we find that \textsc{Pbad} is considerably slower and requires more than 1.5 hours to complete on each \texttt{Water} dataset. The minimum support threshold results in an embedding of about $5\,000$ 
%$250-1\,000$ itemsets and $4\,000-5\,200$ sequential
patterns and most time is needed to create an embedding of those $5\,000$ patterns in $27\,000$ windows. % that result from applying a sliding window of 3 days and an increment of 1 hour. %\textsc{Fpof} suffers from the same drawback as \textsc{Pbad} but typically fewer itemsets are discovered so the runtime is not as problematic. 
In \textsc{IPbad} using default parameters for pattern discovery results in fewer patterns (800 for the \texttt{Water} dataset) and both pattern discovery and the creation of the embedding are more efficient resulting in a total runtime of less than a few minutes. On \texttt{SMD} %which consists of 28 devices and 38 time series per device 
the total time for pre-processing, learning the model and making predictions was just 37 \emph{minutes} on a laptop which is an order of magnitude faster than related deep-learning methods that require expensive clusters of GPU's to achieve a comparable training time. We conclude that \textsc{IPbad} and \textsc{IsoForest} scale to long time series and collections thereof.

    
