import os
import pandas as pd
import numpy as np
import random
from collections import defaultdict
import time

from utils import print_full
from preprocess_timeseries import pre_process_mv_df
from eval_methods import eval_best_f1
from baselines.iso_forest import run_iso_forest_mv
from ipbad.main import run_ipbad_mv
from visualisation import plot_mv_data

data_folder = '/Users/lfereman/git2/conscious/data/multivariate/SMAP/'
files = os.listdir(data_folder) 
datasets = [f[0:f.index('_train.csv')] for f in files if f.endswith('_train.csv')]
print(len(datasets),datasets)
results = defaultdict(list)

machine = datasets[0]
#machine = 'A-1'
machine_1_test = f'{machine}_test.csv'
machine_1_train = f'{machine}_train.csv'
machine_1_test_labels = f'{machine}_labels.csv'

df = pd.read_csv(data_folder + machine_1_test) #i.e. contains metric-1,...,metric-38,time_id,time
df['time'] = pd.to_datetime(df['time'])
labels_df = pd.read_csv(data_folder + machine_1_test_labels) #i.e. contains chan_id,spacecraft,anomaly_sequences,class,num_values
labels_df['anomaly_sequences'] = labels_df['anomaly_sequences'].apply(eval)
anomaly_ranges = labels_df.iloc[0]['anomaly_sequences'] #i.e. [[550, 750], [2100, 2210]]
anomalies = []
for anomaly_range in anomaly_ranges:
    sample = df[(df['time_id'] >= anomaly_range[0]) & (df['time_id'] <= anomaly_range[0])]
    anomalies.extend([row['time'] for idx, row in sample.iterrows()])
print(f'No anomalies: {len(anomalies)}. First: {anomalies[0]}')

df, components = pre_process_mv_df(df)   


preprocess_parameter = {'interval': 8, 'no_symbols': 30, 'no_bins': 30} #no_bins was 5
preprocess_outlier=False

an_scores = run_ipbad_mv(df, interval=preprocess_parameter['interval'], stride=1, preprocess_outlier=preprocess_outlier, 
                         no_symbols=preprocess_parameter["no_symbols"], no_bins=preprocess_parameter["no_bins"], use_iso=True, use_MDL=True)
(f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies)
print(f"IPBAD F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")
(f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies, point_adjust=False)
print(f"IPBAD W/0 F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}, TN:{TN}")

plot_mv_data("machine1", df, anomalies, an_scores, figname=None, threshold=t)

#random search:
random_search = True
if random_search:
    grid={"interval": [4,5,6,7,8],
                "stride": [1],
                "no_symbols": [10,15,20,25,30,35],
                "no_bins" : [15,20,25,30],
                "preprocess_outlier": [False], 
                "use_MDL": [True],
                "binning_method": ['normal'],
                "use_iso": [True]
                }
    no_iter = 150
    best_parameters = None
    best_metrics = None
    best_an_scores = None
    while no_iter > 0:
        random_parameter = {}
        for key, options in grid.items():
            idx = random.randint(0,len(options)-1)
            random_parameter[key] = options[idx]
        try:
            an_scores = run_ipbad_mv(df,  **random_parameter)
            (f1,prec,rec,TP,FP,FN,TN, t) = eval_best_f1(an_scores, anomalies, point_adjust=False)
            print(f"random parameters: {random_parameter}")
            print(f"F1@best: {f1:.4f}, precision: {prec:.4f}, recall: {rec:.4f}, threshold: {t:.4f}, TP: {TP}, FP: {FP}, FN:{FN}")
            no_iter = no_iter - 1
            if best_metrics is None or f1 > best_metrics[0]:
                best_metrics =   (f1,prec,rec,TP,FP,FN,TN, t)
                best_parameters = random_parameter
                best_an_scores = an_scores
        except Exception as e:
            print(e)
    print('#### Best result ####')
    (f1,prec,rec,TP,FP,FN,TN, t) = best_metrics

#random parameters: {'interval': 4, 'stride': 1, 'no_symbols': 12, 'no_bins': 15, 'preprocess_outlier': True, 'use_MDL': True, 'binning_method': 'normal', 'use_iso': True}
#F1@best: 0.5000, precision: 0.3750, recall: 0.7500, threshold: 0.4285, TP: 9, FP: 15, FN:3
#random parameters: {'interval': 4, 'stride': 1, 'no_symbols': 10, 'no_bins': 50, 'preprocess_outlier': True, 'use_MDL': True, 'binning_method': 'normal', 'use_iso': True}
#F1@best: 0.5128, precision: 0.3704, recall: 0.8333, threshold: 0.4344, TP: 10, FP: 17, FN:2
#random parameters: {'interval': 4, 'stride': 1, 'no_symbols': 18, 'no_bins': 15, 'preprocess_outlier': True, 'use_MDL': True, 'binning_method': 'normal', 'use_iso': True}
#F1@best: 0.5405, precision: 0.4000, recall: 0.8333, threshold: 0.4643, TP: 10, FP: 15, FN:2
#random parameters: {'interval': 8, 'stride': 1, 'no_symbols': 30, 'no_bins': 30, 'preprocess_outlier': False, 'use_MDL': True, 'binning_method': 'normal', 'use_iso': True}
#F1@best: 0.7333, precision: 0.6111, recall: 0.9167, threshold: 0.4373, TP: 22, FP: 14, FN:2
