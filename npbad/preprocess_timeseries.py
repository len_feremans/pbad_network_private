import math
import time
import os
from datetime import datetime

import pandas as pd
import numpy as np
from numpy import linalg as LA
from tqdm import tqdm
import matplotlib.pyplot as plt
import seaborn as sns
from collections import defaultdict
    
#1.2) Load and show time series
def load_ts(filename):
  df_ts = pd.read_csv(filename)
  df_ts['time'] = pd.to_datetime(df_ts['time'])
  return df_ts
              
#sample ts values (Bresenham interpolation)
def sample_ts(df_ts, col='average_value', values=2000):
  win = float(df_ts.shape[0] / values)
  times_c = df_ts['time'].values
  vals_c = df_ts[col].values
  output = []
  i = 0
  while i <  df_ts.shape[0]:
    output.append([vals_c[int(i)], times_c[int(i)]])
    i+= win
    if len(output) == values:
      break
  df_sampled = pd.DataFrame(output,columns=[col,"time"])
  return df_sampled


def remove_outliers_ts(ts, q=0.95):
    q99 = ts['average_value'].quantile(q)
    if q99 == ts['average_value'].quantile(q-0.10): #i.e. flat line...
        return ts
    m = ts['average_value'].mean()
    i = 0
    for idx, row in ts[ts['average_value'] > q99].iterrows():
        before = ts[ts['time'] < row['time']]
        i=i+1
        if before.shape[0] > 0:
            #see https://stackoverflow.com/questions/23330654/update-a-dataframe-in-pandas-while-iterating-row-by-row
            #row['average_value'] = before.iloc[-1]['average_value']
            ts.at[idx,'average_value'] = before.iloc[-1]['average_value']
        else:
            #row['average_value'] = m
            ts.at[idx,'average_value'] = m
    print(f'remove outlier: replaced {i} values')
    return ts

def remove_outliers_ts_new(ts, q=0.99):
    '''
    Find outliers > q=0.99 and < 1=1-q 
    Cap value, i.e. quantile 99
    '''
    q99 = ts['average_value'].quantile(q)
    q01 = ts['average_value'].quantile(1-q)
    if q99 == ts['average_value'].quantile(q-0.10): #i.e. flat line...
        return ts
    if q01 == ts['average_value'].quantile(1-q + 0.10): #i.e. flat line...
        return ts
    ts.loc[ts['average_value'] > q99,'average_value'] = q99
    ts.loc[ts['average_value'] < q01,'average_value'] = q01
    return ts

def min_max_norm(my_ts):
  ts = my_ts.copy()
  if ts['average_value'].max() == 0:
      return ts
  ts['average_value'] = (ts['average_value'].values -  ts['average_value'].min()) / (ts['average_value'].max() - ts['average_value'].min())
  return ts


#load all 8000 time series and create paa presentation
def min_max_norm_inplace(ts):
  ts['average_value'] = (ts['average_value'].values -  ts['average_value'].min()) / (ts['average_value'].max() - ts['average_value'].min())
  return ts
  
def z_norm_ts(df_ts, col='average_value'):
  v1 = _z_norm(df_ts[col].values)
  df_ts_new = df_ts.copy()
  df_ts_new[col] = v1
  return df_ts_new


def _z_norm(values, threshold_std = 0.01):
  #Like in SAX/PETSC: If the standard deviation is below a certain threshold, typically set to 0.01, we do not z-normalise to prevent that noise is amplified.
  #print(values)
  v1 = values - np.mean(values)
  if np.std(values) < threshold_std:
    return v1
  if LA.norm(v1) != 0:
    v1 = v1 /  LA.norm(v1)
  return v1



def check_and_fix_equal_length_segments(segments):
    #check segment sizes
    #for i, segment in enumerate(segments):
    #    if segment.shape[0] != last_segment_size:
    #        print(f'segment {i}: len: {segment.shape[0]} expected: {last_segment_size}')
    #fix
    last_segment_size = segments[0].shape[0]
    for i, segment in enumerate(segments):
        if segment.shape[0] != last_segment_size:
            if segment.shape[0] != last_segment_size and abs(segment.shape[0] - last_segment_size) > 1:
                #i.e. for colruyt
                new_segment = [0 for i in range(0,last_segment_size)]
                segment = segment.tolist() 
                for j in range(0, min(len(segment),last_segment_size)):
                    new_segment[j] = segment[j]
                segments[i] = np.array(new_segment)
            elif segment.shape[0] > last_segment_size:
                segments[i] = np.array(segment.tolist()[0:last_segment_size]) #works if slightly larger for some segments (i.e. 1 longer, because of rounding
            elif segment.shape[0] < last_segment_size:
                segments[i] = np.array(segment.tolist() + [segment.tolist()[-1]])
    for i, segment in enumerate(segments):
        if segment.shape[0] != last_segment_size:
            print(f'segment {i}: len: {segment.shape[0]} expected: {last_segment_size}')
    return segments

  
def fill_missing(df_ts, gap_in_minutes=None):
  #assumes input has time and average_value cols
  prev = None
  output = []
  if gap_in_minutes is None:
      gap_in_minutes = (df_ts.iloc[1]['time'] - df_ts.iloc[0]['time']).seconds / 60 
  for idx, row in df_ts.iterrows():
    t = row['time']
    v = row['average_value']
    if prev == None:
      prev = t
      output.append([v,t])
      continue
    delta = (t - prev).seconds / 60
    if delta <= gap_in_minutes + 0.5:
      output.append([v,t])
      prev = t
      continue
    else:
      while prev < t:
        prev = prev + np.timedelta64(int(gap_in_minutes),'m')
        output.append([v,prev])
      #output.append([v,t])
  df_ts_new = pd.DataFrame(output, columns=['average_value','time'])
  rel_gain = (df_ts_new.shape[0] - df_ts.shape[0])/df_ts.shape[0] * 100
  print(f"fill_missing: adding (relative) +{rel_gain}% more values (from {df_ts.shape[0]} to {df_ts_new.shape[0]})")
  #display(df_ts_new)
  return df_ts_new
  
  
def paa_time_based(ts, interval_minutes=24):
   times = []
   paa_vals = []
   m = ts['average_value'].mean()
   min_date_c = ts['time'].min()
   max_date_c = ts['time'].max()
   while min_date_c < max_date_c:
     next = min_date_c + np.timedelta64(interval_minutes,'m')
     day = ts[(ts['time'] >= min_date_c) & (ts['time'] < next)]
     values_day = day['average_value'].values
     if len(values_day) == 0 & len(paa_vals) == 0:
       paa_vals.append(m)
     elif len(values_day) == 0:
       paa_vals.append(paa_vals[-1])
     else:
       paa_vals.append(np.mean(values_day))
     times.append(min_date_c + np.timedelta64(interval_minutes//2,'m') )
     min_date_c = next
   return pd.DataFrame.from_records(list(zip(times,paa_vals)), columns=['time','average_value'])


#padding for boundaries
def padd(df_ts, min_date, max_date, value=0, col='average_value',size=None,ref_ts=None):
  output = []
  mini = df_ts['time'].min()
  maxi = df_ts['time'].max()
  if value == 'mean':
    value = df_ts[col].mean()
  while min_date < mini:
    output.append([value,min_date])
    min_date = min_date + np.timedelta64(5,'m')
  for idx, row in df_ts.iterrows():
    t = row['time']
    v = row[col]
    output.append([v,t])
  while maxi <= max_date:
    maxi = maxi + np.timedelta64(5,'m')
    output.append([value,maxi])
  df_ts_new = pd.DataFrame(output, columns=[col,"time"])
  if not size is None:
      #i.e. due to rounding/sampling errors maybe 1 value needed to be added to beginning or end
      if df_ts_new.shape[0] != size:
       add_one_at_start = abs(df_ts_new['time'].values[0] - ref_ts['time'].values[0]) > abs(df_ts_new['time'].values[-1] - ref_ts['time'].values[-1])
       if df_ts_new.shape[0] < size: 
          if add_one_at_start:
              df2 = pd.DataFrame({'average_value':[value], "time": df_ts_new['time'].values[0] - np.timedelta64(5,'m')})
              df_ts_new = df2.append(df_ts_new)
              df_ts_new = df_ts_new.reset_index(drop=True)
          else:
              df2 = pd.DataFrame({'average_value':[value], "time": df_ts_new['time'].values[-1] + np.timedelta64(5,'m')})
              df_ts_new = df_ts_new.append(df2)
              df_ts_new = df_ts_new.reset_index(drop=True)
       else: #drop one 
          if add_one_at_start:
              df_ts_new = df_ts_new.iloc[1:,:]
          else:
              df_ts_new = df_ts_new.iloc[:-1,:]
  return df_ts_new


def _fit_line(ts_values):  
  y = ts_values
  x = range(0,len(y))
  m, b = np.polyfit(x, y, 1)
  y_new = x * m + b
  return y_new 
  
def pearson(y,y_new):
  v1 = _z_norm(y)
  v2 = _z_norm(y_new)
  corr = v1.dot(v2)
  return corr

def is_straight(ts):
  y_new = _fit_line(ts)
  d = np.linalg.norm(ts - y_new)
  if d < 0.001: #special case: all 0 lines, correlation is 0
     return True
  if pearson(ts,y_new) > 0.98:
    return True
  else:
    return False


def pre_process_mv_df(df,t1=0.95,t2=0.85):
    ''' Does two things:
    1) Remove time series correlated where pearson correlation coefficient > 0.95 
    2) Cluster (remaining time series) correlated pearson correlation coefficient > 0.85 (cluster means rename column X to X_cluster_I)
    '''
    #create list of time series
    metrics = [col for col in df.columns if col != 'time']
    ts_data = [df[metric].values for metric in metrics]
    #1) Find time series that are flat or straight line
    straight_ts = list()
    for i, ts in enumerate(ts_data):
        if is_straight(ts):
            straight_ts.append(i)
    #2) Find time series that are perfectly correlated 
    corr_m = find_similarities_pearson_simple(ts_data, threshold = t1)
    connected_components = find_connected_components(corr_m)
    #3) Remove
    metric_straight = [metrics[i] for i in straight_ts]
    perfect_correlated = []
    for component in connected_components:
        component_lst = sorted(list(component))
        for j in component_lst[1:]:
            perfect_correlated.append(metrics[j])
    keep = [metric for metric in metrics if metric not in metric_straight and metric not in perfect_correlated]
    print(f'Removing: Before: {len(metrics)}->{len(keep)} straight: {metric_straight} and perfect correlated: {perfect_correlated}')
    df = df[['time'] + keep]
    # Group remaining time series on correlation -> pass as return value
    ts_data = [df[metric].values for metric in keep]
    corr_m = find_similarities_pearson_simple(ts_data, threshold = t2)
    almost_connected_components = find_connected_components(corr_m)
    almost_connected_components = [sorted([keep[i] for i in component]) for component in almost_connected_components]
    almost_connected_components.sort(key=lambda s: len(s)) #short small to large component
    print(almost_connected_components)
    #Sort and rename columns
    #first columns not in any component
    ordered_columns =[]
    for col in keep:
        in_component = False
        for comp in almost_connected_components:
            if col in comp:
                in_component=True
        if not in_component:
            ordered_columns.append(col)
    for component in almost_connected_components:
        for col in component:
            ordered_columns.append(col)
    df = df[['time'] +ordered_columns]
    #rename
    mapper = {}
    for i, component in enumerate(almost_connected_components):
        for col in component:
            mapper[col] = col + f"-C{i+1}"
    df = df.rename(columns=mapper)
    return df, almost_connected_components
    

#assumes list of time series values
def find_similarities_pearson_simple(data_ts, threshold = 0.95):
  start = time.time()
  corr_m = defaultdict(dict)
  pairs = []
  for i in range(0, len(data_ts)):
    for j in range(i+1, len(data_ts)):
      pairs.append((i,j))
  for i,j in tqdm(pairs, total=len(pairs),  position=0, leave=True):
      corr = pearson(data_ts[i],data_ts[j])
      if abs(corr) > threshold: #also -1 if complete reverse correlations
        corr_m[i][j] = corr
        #print('corr {} {} : {:.3f}'.format(i,j,corr))
  print(f'find_similarities_pearson_simple: elapsed: {time.time() -start}s')
  return corr_m
  
#Find connected components in the correlation graph
def find_connected_components(sim_matrix):
  cliques = []
  for i in sim_matrix.keys():
    j = sim_matrix[i].keys()
    clique_find = None
    for clique in cliques:
      if i in clique:
        clique_find = clique
        break
      if len(clique.intersection(j)) != 0:
        clique_find = clique
        break
    if clique_find is None:
      clique_find = set()
      clique_find.add(i)
      cliques.append(clique_find)
    for j_ts in j: 
      clique_find.add(j_ts)
    clique_find.add(i)
  print(f"found {len(cliques)} components: {cliques}")
  return cliques