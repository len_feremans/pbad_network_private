import numpy as np
import pandas as pd
from sklearn import cluster
from pyts.approximation import PiecewiseAggregateApproximation

def my_paa(segment, no_symbols):
    if len(segment) < no_symbols:
        #print(f"warn: segment {segment} length is lower than {no_symbols}.")
        return segment
    #SLOW
    #from saxpy.paa import paa
    #from saxpy.alphabet import cuts_for_asize
    #from saxpy.sax import ts_to_string
    #segment_to_paa = paa(segment,no_symbols)
    paa_class = PiecewiseAggregateApproximation(window_size=None, output_size=no_symbols)
    segment_r = segment.reshape(1, -1) 
    segment_to_paa = paa_class.transform(segment_r)[0] 
    return segment_to_paa

def create_windows(ts, interval=24, stride=1):
    min_date_c = ts['time'].min()
    max_date_c = ts['time'].max()
    windows = []
    while min_date_c < max_date_c:
        end = min_date_c + np.timedelta64(interval,'h')
        if end > max_date_c: #skip smaller window at end...
            break
        windows.append((min_date_c, end))
        min_date_c = min_date_c + np.timedelta64(stride,'h')
    return windows

def create_segments(ts, windows):    
    segments = list()
    for win in windows:
        segment = ts[(ts['time'] >= win[0]) & (ts['time'] < win[1])]['average_value'].values #TODO: not dropping last value of last segment
        segments.append(segment)
    return segments

def discretise_segments_equal_distance_bins_global(segments, no_bins=5, no_symbols=10):
    bins = [(i/no_bins, (i+1)/no_bins)for i in range(0,no_bins)]
    bins[0] = (bins[0][0] - 0.01, bins[0][1])
    bins[-1] = (bins[-1][0], bins[-1][1] + 0.01)
    bins_pandas = pd.IntervalIndex.from_tuples(bins, closed='left')
    segments_discretised = []
    for segment in segments:
        try:
            segment_to_paa = my_paa(segment, no_symbols)
        except ValueError as e:
            print(e)
            print(segment)
        d = pd.cut(segment_to_paa,bins_pandas)
        d = d.codes
        segments_discretised.append(d)
    return segments_discretised 

#per segment binning, similar to SAX
def discretise_segments_equal_distance_bins_local(segments, no_bins=5, no_symbols=10):
    print('local discretisation')
    segments_discretised = []
    for segment in segments:
        segment_to_paa = my_paa(segment,no_symbols)
        d = pd.cut(segment_to_paa,no_bins,labels=False) #local i.e. range is determined locally
        segments_discretised.append(segment_to_paa)
    return segments_discretised

#use k-means instead of assuming normal distribution 
def discretise_segments_kmeans(ts_i, segments, no_bins=5, no_symbols=10, verbose=False):
    k_means = cluster.KMeans(n_clusters=no_bins)
    X = ts_i['average_value'].values.reshape(-1,1)  
    k_means.fit(X)
    if verbose:
        print(f'k-means centers: {k_means.cluster_centers_}')
    segments_discretised = []
    for segment in segments:
        segment_to_paa = my_paa(segment,no_symbols) 
        segment_to_paa = segment_to_paa.reshape(-1,1)
        d = k_means.predict(segment_to_paa)
        segments_discretised.append(d)
    _reorder_clusters_low_to_high(k_means.cluster_centers_, segments_discretised)
    return segments_discretised


def _reorder_clusters_low_to_high(cluster_centers, segment_discretised):
    #K-means does not "sort" cluster centers, i.e. high or low values are in random position
    #i.e. centers are [[0.96298271], [0.01163506]. [0.49297885], [0.2378306], [0.74126472]]
    #and we want encode to be sorted from low to high values
    cluster_centers = [(idx, point) for idx, point in enumerate(cluster_centers)]
    cluster_centers.sort(key=lambda t: t[1]) #sort from low to high
    d = {}
    for idx, t in enumerate(cluster_centers):
        d[t[0]] = idx
    for segment_d in segment_discretised:
        for i, old_label in enumerate(segment_d):
            segment_d[i] = d[old_label] #inplace
            