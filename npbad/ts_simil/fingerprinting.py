import math
import numpy as np
import time
import matplotlib.pyplot as plt
from collections import defaultdict
from tqdm import tqdm


def _d_to_arr(d, dim_d):
  t1 = []
  for i in range(0, dim_d):
     for j in range(i+1, dim_d):
         if i in d and j in d[i]:
             t1.append(d[i][j])
  return np.array(t1)

 
 
#Fingerprinting
def make_grid_ts(ts, x_steps, y_steps):
  #create y_windows
  window_f = ts.shape[0] / float(y_steps)
  windows = []
  i_f = 0
  while math.ceil(i_f) < ts.shape[0]:
    next = i_f + window_f #bresenham kind-of-logic
    windows.append([math.ceil(i_f), math.ceil(next)])
    i_f = next
  #create  x_windows:
  x_vals = [i / x_steps for i in range(0,x_steps+1)] #i.e. 5 -> 0,0.2,0.4,0.6,0.8,1.0 and >= 0 an < 0.2 ... 0.8>= and <= 1.0 (last one is wqu)
  #make grid and count values in each cell, resolution is x * y
  m = np.ones(shape=(x_steps,y_steps))
  for j, window in enumerate(windows):
    a = ts['average_value'].values[window[0]:window[1]]
    for i in range(0,x_steps):
      lower  = x_vals[i]
      higher = x_vals[i+1]
      if i == x_steps - 1:
        higher = higher + 1 #i.e. above 0.8 
      mask = np.where((a >= lower) & (a < higher))
      m[x_steps-i-1][j] = len(a[mask])
  return m


def transform_m(m, fill_down=False):
  m = np.log2(m + 1) #or log2? #FOUND BUG log(10) needs + 10, using LOG2 now...
  #fill lines
  #TODO: with/without fill lines...
  if fill_down:
    for i in range(0,m.shape[0]):
      for j in range(0,m.shape[1]):
        if m[i][j] > 0:
          for k in range(i+1, m.shape[0]):
             m[k][j] = max(m[k][j],m[i][j])
  return m

def distance_m(m1,m2):
  return np.sqrt(np.square((m1 - m2)).sum())

"""##Fingerprinting RLE"""

#for data with in-equal sampling due to compression (i.e. only storing values that are different), use this
def create_windows_RLE(ts, min_date_c, max_date_c, interval_hours=24):
   windows = []
   while min_date_c < max_date_c:
     next = min_date_c + np.timedelta64(interval_hours,'h')
     indexes = ts.index[(ts['time'] >= min_date_c) & (ts['time'] < next)]
     if indexes.shape[0] != 0:
        windows.append((indexes[0],indexes[-1]))
     else:
        windows.append(-1)
     min_date_c = next
   return windows

def finger_printing_RLE(ts, x_steps, min_date_c, max_date_c, interval_hours=24):
  windows = create_windows_RLE(ts, min_date_c, max_date_c, interval_hours)
  x_vals = [i / x_steps for i in range(0,x_steps+1)] #i.e. 5 -> 0,0.2,0.4,0.6,0.8,1.0 and >= 0 an < 0.2 ... 0.8>= and <= 1.0 (last one is wqu)
  y_steps = len(windows)
  #make grid and count values in each cell, resolution is x * y
  m = np.zeros(shape=(x_steps,y_steps))
  for j, window in enumerate(windows):
    if window == -1:
      continue #just keep zero, was mean or past value
    a = ts['average_value'].values[window[0]:window[1]]
    for i in range(0,x_steps):
      lower  = x_vals[i]
      higher = x_vals[i+1]
      if i == x_steps - 1:
        higher = higher + 1 #i.e. above 0.8 
      mask = np.where((a >= lower) & (a < higher))
      m[x_steps-i-1][j] = len(a[mask])
  m = transform_m(m)
  return m
  

"""##Histogram distance"""

def histogram(ts1, no_bins=100):
  bins = np.arange(0, 1, 1/float(no_bins))
  hist1, _ = np.histogram(ts1['average_value'].values, bins=bins)
  return hist1 / float(ts1.shape[0]) #DIFFERENT FROM paper

def histogram_distance(h1,h2):
  d =  np.abs(h1 - h2).sum()
  return d
  
  
""" All pairs similarities """
  
def find_similarities_fingerprinting(data_ts_all, data_ts_paa, data_ts_fp, data_ts_hist, sim_method='fp_hist', q_f=0.01, q_t=0.01):
   start = time.time() 
   dist_m = defaultdict(dict)
   dist_m2 = defaultdict(dict)
   pairs = []
   for i in range(0, len(data_ts_all)):
     for j in range(i+1, len(data_ts_all)):
       pairs.append([i,j])
   for i,j in tqdm(pairs, total=len(pairs)):
       dist = distance_m(np.array(data_ts_fp[i]),np.array(data_ts_fp[j]))
       dist_2 = histogram_distance(data_ts_hist[i],data_ts_hist[j])
       dist_m[i][j] = dist
       dist_m[j][i] = dist #store both directions
       dist_m2[i][j] = dist_2
       dist_m2[j][i] = dist_2 #store both directions
   #apply threshold:
   t1 =  np.quantile(_d_to_arr(dist_m, len(data_ts_all)),q=q_f)
   t2 =  np.quantile(_d_to_arr(dist_m2, len(data_ts_all)),q=q_t)
   print(f'threshold fp: {t1:.3f} threshold hist: {t2:.3f}')
   dist_t = defaultdict(dict)
   for i,j in tqdm(pairs, total=len(pairs)):
       if dist_m[i][j]  <=t1 and dist_m2[i][j] <=t2:
          dist_t[i][j] = dist_t[j][i] = dist_m[i][j]
   total_edges = []
   for i in  tqdm(dist_t.keys(), total=len(dist_t.keys())):
      nearest = sorted(list(dist_t[i].items()), key=lambda p: p[1])
      total_edges.append(len(nearest))
      dist_t[i] = nearest
   print(f'On average {np.mean(total_edges)} edges between time series. Total relations: {np.sum(total_edges)}')
   print(f'find similarities: elapsed: {time.time() -start}s') 
   return dist_t


  
def plot_grid(m,fname=None):
  fig, ax = plt.subplots(figsize=(7,1))
  from matplotlib.colors import Normalize
  norm=Normalize()
  plt.imshow(m, norm=norm, cmap='gray_r')
  plt.axis('tight')
  if fname is None:
      plt.show()
  else:
      plt.axis('off')
      plt.savefig(fname, bbox_inches='tight')
      plt.close()

def histogram_distance_vis(ts1,ts2,no_bins=100,title=''):  
  bins = np.arange(0, 1, 1/float(no_bins))
  plt.title(title)
  plt.hist(ts1['average_value'].values, bins=bins, alpha = 0.5)
  plt.hist(ts2['average_value'].values,  bins=bins, alpha = 0.5)
  d = histogram_distance(histogram(ts1,no_bins), histogram(ts2,no_bins))
  print(f'hist dist: {d:0.3f}')
  plt.show()
 