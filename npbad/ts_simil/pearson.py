import math
import numpy as np
import time
import matplotlib.pyplot as plt
from collections import defaultdict
from tqdm import tqdm
import seaborn as sns

from npbad.preprocess_timeseries import _z_norm, load_ts, pearson, min_max_norm_inplace
from npbad.ts_simil.fingerprinting import _d_to_arr

 
#def load_ts_and_process_fast(filename, min_date, max_date):
#  ts = load_ts(filename)
#  ts = ts[(ts['time']>= min_date) & (ts['time']<= max_date)]
#  ts = min_max_norm_inplace(ts)
#  return ts

#assumes list of time series
def find_similarities_pearson(data_ts_all, q_p=0.99, threshold=None):
   start = time.time() 
   dist_m = defaultdict(dict)
   pairs = []
   data_ts_normalised = []
   for data_ts_i in tqdm(data_ts_all, total=len(data_ts_all)):
       v1 = _z_norm(data_ts_i['average_value'].values)
       data_ts_normalised.append(v1)
   for i in range(0, len(data_ts_all)):
     for j in range(i+1, len(data_ts_all)):
       pairs.append([i,j])
   for i,j in tqdm(pairs, total=len(pairs)):
       #dist = abs(data_ts_normalised[i].dot(data_ts_normalised[j]))
       dist = data_ts_normalised[i].dot(data_ts_normalised[j])
       if np.isnan(dist):
           continue
       dist_m[i][j] = dist
       dist_m[j][i] = dist #store both directions
   #apply threshold:
   if threshold is None:
       t1 =  np.quantile(_d_to_arr(dist_m, len(data_ts_all)),q=q_p)
   else:
       t1 = threshold     
   print(f'threshold pearson: {t1:.3f}')
   dist_t = defaultdict(dict)
   for i,j in tqdm(pairs, total=len(pairs)):
       if i in dist_m and j in dist_m[i] and dist_m[i][j]  >= t1:
          dist_t[i][j] = dist_t[j][i] = dist_m[i][j]
   total_edges = []
   for i in  tqdm(dist_t.keys(), total=len(dist_t.keys())):
      nearest = sorted(list(dist_t[i].items()), key=lambda p: p[1], reverse=True)
      total_edges.append(len(nearest))
      dist_t[i] = nearest
   print(f'On average {np.mean(total_edges)} edges between time series. Total relations: {np.sum(total_edges)}')
   print(f'find similarities pearson: elapsed: {time.time() -start}s') 
   return dist_t
