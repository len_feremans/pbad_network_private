import os, subprocess, sys, time
from datetime import datetime
from sys import stderr
import json
import pandas as pd
import numpy as np
from npbad.preprocess_timeseries import fill_missing


### UNIVARIATE
def load_numenta_data(file, labels, key=None):
    df = pd.read_csv(file) #contains timestamp,value
    df['timestamp'] = pd.to_datetime(df['timestamp'])
    df = df.rename(columns={"timestamp":"time", "value":"average_value"})
    df = fill_missing(df)     #i.e. for ambient data
    labels_dct = json.loads(open(labels,'r').read()) #i.e. key is realKnownCause/nyc_taxi.csv", contains list of timestamps
    if key is None:
        key = file.split('/')[-2] + '/' + file.split('/')[-1]
    anomalies = [np.datetime64(anomaly) for anomaly in labels_dct[key]]
    print(f'loading data. start: {df["time"].iloc[0]}. end: {df["time"].iloc[-1]}. interval: {(df["time"].iloc[1] - df["time"].iloc[0]).seconds /60}m')
    print(f'anomalies: #{len(anomalies)}. timestamps: {anomalies}')
    return df, anomalies

def load_colruyt_data(file):
    df = pd.read_csv(file) #contains timestamp,value
    df['datum'] = pd.to_datetime(df['datum'])
    df = df.rename(columns={"datum":"time", "usage_m3":"average_value"})
    anomalies = [row['time'] for idx, row in df[df['label']==1.0].iterrows()]
    print(f'loading data. start: {df["time"].iloc[0]}. end: {df["time"].iloc[-1]}. interval: {(df["time"].iloc[1] - df["time"].iloc[0]).seconds /60}m')
    return df[['time','average_value']], anomalies


def run_cmd(cmd, output_fname, verbose=False):
    try:
        if os.path.exists(output_fname):
            os.remove(output_fname)
        if verbose:
             subprocess.call(cmd)
        else:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate(timeout=600)
            str_err = err.decode("utf-8")
            if len(str_err) > 0:
                stderr.write(str_err) 
                return 1
        return 0
    except Exception as e:
        stderr.write(str(e))
        stderr.write("process aborted")
        return 1
    #run scikir learn classification
    if os.stat(output_fname).st_size==0: #no patterns found -> file is empty
        stderr.write("output empty")
        return 1

#check https://stackoverflow.com/questions/25351968/how-can-i-display-full-non-truncated-dataframe-information-in-html-when-conver
def print_full(x, k=50):
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', 2000)
    pd.set_option('display.float_format', '{:20,.2f}'.format)
    pd.set_option('display.max_colwidth', None)
    print(x.head(k))
    pd.reset_option('display.max_rows')
    pd.reset_option('display.max_columns')
    pd.reset_option('display.width')
    pd.reset_option('display.float_format')
    pd.reset_option('display.max_colwidth')
    print(f"total #{x.shape[0]} rows")
    
def cart_product(**kwargs):
    parameters = [{}]
    for key, values in kwargs.items():
        new_parameters = []
        for par_dct in parameters:
            for val in values:
                new_par_dct = par_dct.copy()
                new_par_dct[key] = val
                new_parameters.append(new_par_dct) 
        parameters = new_parameters
    return parameters