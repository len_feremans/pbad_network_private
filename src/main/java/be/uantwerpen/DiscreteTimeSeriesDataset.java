package be.uantwerpen;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import be.uantwerpen.util.CountMap;

public class DiscreteTimeSeriesDataset{
	public int[][] features;
	public int[] labels;
	public int[] originalTimeseriesIdx; //keep mapping to original time series after mapping
	public float[] weights; //for soft-cover
	
	public DiscreteTimeSeriesDataset copy() {
		DiscreteTimeSeriesDataset ts_new = new DiscreteTimeSeriesDataset();
		ts_new.features = new int[features.length][];
		ts_new.labels = new int[labels.length];
		for(int i=0; i<features.length; i++) {
			ts_new.features[i] = Arrays.copyOf(features[i], features[i].length);
			ts_new.labels[i] = labels[i];
		}
		return ts_new;
	}
	
	public Set<Integer> unique(){
		TreeSet<Integer> set = new TreeSet<>();
		for(int i=0; i< features.length; i++) {
			for(int j=0; j < features[i].length; j+=1) {
				set.add(features[i][j]);
			}
		}
		set.remove(-1); //-1 is the "gap" element -> See also MineSequentialPatternsDirectCover
		return set;
	}
	
	public CountMap<Integer> labelCounts(){
		CountMap<Integer> set = new CountMap<>();
		for(int i=0; i< labels.length; i++) {
			set.add(labels[i]);
		}
		return set;
	}
	
	public void save(File file) throws IOException {
		save(file,false, false);
	}
	
	public void save(File file, boolean saveLabel, boolean saveTimeSeriesId) throws IOException {
		if(!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		FileWriter writer = new FileWriter(file);
		for(int i=0; i<features.length; i++) {
			int[] current_ts = features[i];
			if(current_ts.length==0) {
				continue;
			}
			for(int j=0; j < current_ts.length-1; j++) {
				writer.write(String.valueOf(current_ts[j]));
				writer.write(",");
			}
			writer.write(String.valueOf(current_ts[current_ts.length-1]));
			if(saveLabel) {
				writer.write(":");
				writer.write(String.valueOf(labels[i]));
			}
			if(saveTimeSeriesId) {
				writer.write(":");
				writer.write(String.valueOf(originalTimeseriesIdx[i]));
			}
			if(i!=features.length-1)
				writer.write("\n");
		}
		writer.close();
		System.out.println("Saved " + file.getName());
	}
	
	public void saveAsTS(File file, boolean saveLabel) throws IOException {
		if(!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		FileWriter writer = new FileWriter(file);
		/**
		 * Save header,e,g:
		@problemName Data from Lin for clustering 13 pairs of pre-normalized. time series.
		@timeStamps false
		@missing false
		@univariate true
		@equalLength false
		@classLabel true 0 1 2 3 4 5 6 7 8 9 10 11 12
		*/
		writer.write("@problemName theproblem\n");
		writer.write("@timeStamps false\n");
		writer.write("@missing false\n");
		writer.write("@univariate true\n");
		boolean equalLength = true;
		for(int i=0; i<features.length; i++) {
			int[] current_ts = features[i];
			if(current_ts.length != features[0].length) {
				equalLength = false;
				break;
			}
		}
		writer.write("@equalLength " + (equalLength?"true":"false") +  "\n");
		if(equalLength)
			writer.write("@seriesLength " + String.valueOf(features[0].length) +  "\n");
		List<String> labelsAsStr = new ArrayList<>();
		for(int label: this.labelCounts().keySet()) {
			labelsAsStr.add(String.valueOf(label));
		}
		writer.write("@classLabel true " + String.join(" ",labelsAsStr) + "\n");
		writer.write("@data\n");
		//save data
		for(int i=0; i<features.length; i++) {
			int[] current_ts = features[i];
			for(int j=0; j < current_ts.length-1; j++) {
				writer.write(String.valueOf(current_ts[j]));
				writer.write(",");
			}
			writer.write(String.valueOf(current_ts[current_ts.length-1]));
			if(saveLabel) {
				writer.write(":");
				writer.write(String.valueOf(labels[i]));
			}
			writer.write("\n");
		}
		writer.close();
		System.out.println("Saved " + file.getName());
	}
}