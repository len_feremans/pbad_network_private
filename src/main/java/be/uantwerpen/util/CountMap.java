package be.uantwerpen.util;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class CountMap<K> {

	private Map<K,Integer> map = new TreeMap<K,Integer>(); //Change to TreeMap
	
	public void add(K key){
		map.put(key, get(key) + 1);
	}
	
	public void add(K key, int k){
		map.put(key, get(key) + k);
	}
	
	public void remove(K key){
		map.remove(key);
	}
	
	public int get(K key){
		Integer i = map.get(key);
		return (i == null)?0:i;
	}
	
	public Map<K,Integer> getMap(){
		return map;
	}
	
	public Set<K> keySet(){
		return map.keySet();
	}
		
	public void clear() {
		map.clear();
	}
	
	public String toString() {
		return String.valueOf(map.entrySet());
	}
}
