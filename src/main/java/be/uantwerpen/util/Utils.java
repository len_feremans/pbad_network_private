package be.uantwerpen.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Utils {
	
	public static void runPython(String pythonFile, String[] args) throws IOException {
		String[] command = new String[args.length+2];
		//command[0] = "/usr/local/bin/python3.7";
		command[0] = "/usr/bin/python3.6";
		if(System.getProperty("os.name").contains("Mac OS")){
			command[0] = "/usr/local/bin/python3.7";
		}
		if(!new File(command[0]).exists()) {
			throw new RuntimeException("be.uantwerpen.util.Utils: python not found (change code!): " + command[0]);
		}
		command[1] = pythonFile;
		for(int i=0;i<args.length; i++){
			command[i+2] = args[i];
		}
		System.out.println(">>Running " + Arrays.toString(command));
		Process proc = Runtime.getRuntime().exec(command);
		BufferedReader stdInput = new BufferedReader(new 
				InputStreamReader(proc.getInputStream()));

		BufferedReader stdError = new BufferedReader(new 
				InputStreamReader(proc.getErrorStream()));

		// Read the output from the command
		String s = null;
		while ((s = stdInput.readLine()) != null) {
			System.out.println(s);
		}
		// Read any errors from the attempted command
		while ((s = stdError.readLine()) != null) {
			System.err.println(s);
		}
	}
	
	public static String lst2str(List<Integer> lst) {
		List<String> lstStr = new ArrayList<>(lst.size());
		for(int i: lst) {
			lstStr.add(String.valueOf(i));
		}
		return String.join(",", lstStr);
	}
	
	public static String arrayToStringNice(double[] arr) {
		StringBuffer buff = new StringBuffer();
		if(arr.length < 10) {
			buff.append("[");
			for(int i=0; i<arr.length-1; i++) {
				buff.append(String.format("%.3f", arr[i]));
				buff.append(", ");
			}
			buff.append(String.format("%.3f", arr[arr.length-1]));
			buff.append("]");
		}
		else {
			buff.append("float[" + arr.length + "]=[");
			for(int i=0; i < 9; i++) {
				buff.append(String.format("%.3f", arr[i]));
				buff.append(", ");
			}
			buff.append("..., ");
			buff.append(String.format("%.3f", arr[arr.length-1]));
			buff.append("]");
		}
		return buff.toString();			
	}
	
	public static String arrayToStringNice(int[] arr) {
		StringBuffer buff = new StringBuffer();
		if(arr.length < 10) {
			buff.append("[");
			for(int i=0; i<arr.length-1; i++) {
				buff.append(String.format("%d", arr[i]));
				buff.append(", ");
			}
			buff.append(String.format("%d", arr[arr.length-1]));
			buff.append("]");
		}
		else {
			buff.append("int[" + arr.length + "]=[");
			for(int i=0; i < 9; i++) {
				buff.append(String.format("%d", arr[i]));
				buff.append(", ");
			}
			buff.append("..., ");
			buff.append(String.format("%d", arr[arr.length-1]));
			buff.append("]");
		}
		return buff.toString();			
	}
	
	public static String getFilenameNoExtension(File file) 
	{
		int idx = file.getName().lastIndexOf(".");
		if(idx == -1)
			return file.getName();
		else 
			return file.getName().substring(0, idx);
	}

	public static List<String> readFile(File file) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		List<String> lines = new ArrayList<String>();
		String current = reader.readLine();
		int line = 1;
		while(current != null)
		{
			if(!current.trim().isEmpty())
				lines.add(current);
			current = reader.readLine(); 
			line++;
		}
		reader.close();
		return lines;
	}

	public static double[] toArray(List<Double> intervals) {
		double[] intervalsArr = new double[intervals.size()];
		for(int i=0; i<intervals.size();i++) {
			intervalsArr[i] = intervals.get(i);
		}
		return intervalsArr;
	}

	/**
	 * E.g. java PATS -bins 10 -paa_win 100 -support 5 -gap_constraint 3  -opatterns ./temp/patterns.txt -odiscrete ./temp/discret_txt
	 * and 
	 * Map<String,String> paramTypes = new HashMap<String,String>();
	 * paramTypes.put("bins", "int");
	 * paramTypes.put("paa_win", "int");
	 * paramTypes.put("support", "int");
	 * paramTypes.put("gap_constraint", "int");
	 * paramTypes.put("opatterns", "file");
	 * paramTypes.put("odiscrete", "file");
	 * 
	 * Will return {"bins",10}, etc.
	 * 
	 * @param args
	 */
	public static Map<String, Object> getParameters(String[] args, Map<String, String> paramTypes) {
		Map<String,Object> vals = new TreeMap<>();
		for(int i=0; i< args.length-1; i++) {
			String token = args[i];
			if(token.startsWith("-")) {
				if(paramTypes.get(token.substring(1)) == null) {
					System.err.println("Error: Unknow argument: " + token);
					System.exit(-1);
				}
				else {
					String param = token.substring(1);
					String type = paramTypes.get(param);
					if(type.equals("int")) {
						try {
							Integer val = Integer.valueOf(args[i+1]);
							vals.put(param, val);
						}
						catch(Exception e) {
							System.err.println("Error: Wrong parameter type, expected " + type + ":" + args[i+1]);
							System.exit(-1);
						}
					}
					else if(type.equals("float")) {
						try {
							Float val = Float.valueOf(args[i+1]);
							vals.put(param, val);
						}
						catch(Exception e) {
							System.err.println("Error: Wrong parameter type, expected " + type + ":" + args[i+1]);
							System.exit(-1);
						}
					}	
					else if(type.equals("bool")) {
						try {
							Boolean val = Boolean.valueOf(args[i+1]);
							vals.put(param, val);
						}
						catch(Exception e) {
							System.err.println("Error: Wrong parameter type, expected " + type + ":" + args[i+1]);
							System.exit(-1);
						}
					}
					else if(type.equals("string")) {
						vals.put(param, args[i+1]);
					}
					else if(type.equals("file")) {
						vals.put(param, args[i+1]);
					}			
				}
			}
		}
		return vals;
	}
	
	public static String milisToStringReadable(long milis){
		if(milis < 1000){
			return String.format("%d ms", milis);
		}
		else if(milis >= 1000 && milis < 60000){
			return String.format("%.1f sec", milis/1000.0);
		}
		else if(milis >= 60 * 1000 && milis < 60 * 60 * 1000){
			return String.format("%.1f min", milis/(60 * 1000.0));
		}
		//if(milis >= 360000){
		return String.format("%.2f h", milis/(3600 * 1000.0));		
	}

}
