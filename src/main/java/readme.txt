Code based on PETSC, see https://bitbucket.org/len_feremans/petsc
Code was copy-pasted except for be.uantwerpen.miner.MineTopKSequentialPatternsNoDiscretisation which is new and
MineTopKSequentialPatterns which was adjusted.

To compile run mvn clean install assembly:single with Java 1.8 maven 3.6.3
A pre-compiled version is stored in lib/petsc-miner-pbad-0.1.0-with-dependencies.jar