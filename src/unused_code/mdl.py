import random
import numpy.linalg
import math
from tqdm import tqdm
from symbolic_representation import *
from llvmlite.binding.dylib import add_symbol

#Three problems:
#1) Optimal sliding window segmentation?
#2) Optimal values SAX? i.e. bins and paa_window -> DONE!
#3) Optimal values top-k and mining constraints
#For 1) & 2) See Towards Optimal Symbolization for Time Series Comparisons by Gavin Smith; James Goulding; Duncan Barrack

#i.e. proposition 1:
#Choose representation that minimizes (For all subsequences in S: som t_i,t_j |dist(ti,tj) - dist(s1,s2)| is minimal for
#s_i,j where is results of window_size + stride + no_bins
#i.e. if number of bins 
def optimal_bins(segments, grid={"no_symbols": list(range(5,20)), "no_bins" : list(range(3,25))}):
    #optimize no_bins
    best_score = 1000000
    best_bins = None
    for no_bins in grid["no_bins"]:
        segments_q = discretise_segments_equal_distance_bins_global(segments, no_bins=no_bins, no_symbols=10)
        score = eval_diff_simil(segments, segments_q, no_bins=no_bins)
        if score < best_score:
            best_score = score
            best_bins = no_bins
        print(f'score:{score:0.3f} no_bins:{no_bins}')
    #optimize no_symbols
    best_score = 1000000
    best_symbols = None
    for no_symbols in grid["no_symbols"]:
        segments_q = discretise_segments_equal_distance_bins_global(segments, no_bins=best_bins, no_symbols=no_symbols)
        score = eval_diff_simil(segments, segments_q, no_bins=no_bins)
        if score < best_score:
            best_score = score
            best_symbols = no_symbols
        print(f'score:{score:0.3f} no_symbols:{no_symbols}')
    print(f'best_score:{best_score:.3f} no_symbols:{best_symbols} no_bins:{best_bins}')
    return best_symbols, best_bins

def optimal_bins_grid(segments, grid={"no_symbols": list(range(5,20)), "no_bins" : list(range(3,25))}):
    #optimize both
    best_score = 1000000
    best_symbols = None
    best_bins = None
    for no_symbols in grid["no_symbols"]:
        for no_bins in grid["no_bins"]:
            segments_q = discretise_segments_equal_distance_bins_global(segments, no_bins=no_bins, no_symbols=no_symbols)
            score = eval_diff_simil(segments, segments_q, no_bins=no_bins)
            if score < best_score:
                best_score = score
                best_symbols = no_symbols
                best_bins = no_bins
                print(f'best_score:{score:0.3f} no_symbols:{no_symbols} no_bins:{no_bins}')
    print(f'best:{(best_symbols, best_bins)} score:{best_score:.3f}')
    return best_symbols, best_bins

def optimal_bins_and_interval(ts_i, grid={"interval": list(range(12,24*3,12)), 
                                          "no_symbols": list(range(5,20)), 
                                          "no_bins" : list(range(3,25))}):
    #optimize interval
    best_score = 1000000
    best_interval = None
    for interval in grid["interval"]:
        windows = create_windows(ts_i, interval=interval, stride=1)
        segments = create_segments(ts_i, windows)
        segments_q = discretise_segments_equal_distance_bins_global(segments, no_bins=5, no_symbols=10)
        score = eval_diff_simil(segments, segments_q, no_bins=5)
        if score < best_score:
            best_score = score
            best_interval = interval
        print(f'score:{score:0.3f} interval:{interval}')
    #optimize  bins/no_symbols
    windows = create_windows(ts_i, interval=interval, stride=1)
    segments = create_segments(ts_i, windows)
    best_symbols, best_bins = optimal_bins(segments, grid)
    segments_q = discretise_segments_equal_distance_bins_global(segments, no_bins=best_bins, no_symbols=best_symbols)
    best_score = eval_diff_simil(segments, segments_q, no_bins=best_bins)
    print(f'best interval:{best_interval} best no_symbols:{best_symbols} best no_bins:{best_bins} score:{best_score:.3f}')
    return best_interval, best_symbols, best_bins


def optimal_bins_and_interval_grid(ts_i, 
                                   grid={"interval": list(range(12,24*3,12)), 
                                         "no_symbols": list(range(5,20)), 
                                         "no_bins" : list(range(3,25))}):
    #optimize both
    best_score = 1000000
    best_interval = None
    best_symbols = None
    best_bins = None
    for interval in grid["interval"]:
        windows = create_windows(ts_i, interval=interval, stride=1)
        segments = create_segments(ts_i, windows)
        best_score_interval = 1000000
        for no_symbols in grid["no_symbols"]:
            for no_bins in grid["no_bins"]:
                print('*',end='')
                segments_q = discretise_segments_equal_distance_bins_global(segments, no_bins=no_bins, no_symbols=no_symbols)
                score = eval_diff_simil(segments, segments_q, no_bins=no_bins)
                if score < best_score_interval:
                    best_score_interval = score
                    best_symbols = no_symbols
                    best_bins = no_bins
                    #for debugging
                if score < best_score:
                    best_score = score
                    best_symbols = no_symbols
                    best_bins = no_bins
                    best_interval = interval
                print(f'score:{score:0.3f}  interval {interval}: no_symbols:{no_symbols} no_bins:{no_bins}')
    print(f'best interval:{best_interval} best no_symbols:{best_symbols} best no_bins:{best_bins} score:{best_score:.3f}')
    return best_interval, best_symbols, best_bins


           
def eval_diff_simil(segments, segments_d, iter2=100, no_bins=None, scale_1_0=True):
    #eval: minimal difference in distances between subsequences in the symbolic and continuous space
    score = 0
    random.seed(0.314)
    for i in range(0,iter2): #random search to avoid number of options
        #random segment i and j
        r1 = random.randint(0, len(segments)-1)
        r2 = random.randint(0, len(segments)-1)
        if len(segments[r1]) != len(segments[r2]):
            i = i-1
            continue
        d_org = numpy.linalg.norm(segments[r1] - segments[r2])
        d_symbolic = numpy.linalg.norm(segments_d[r1] - segments_d[r2])
        if scale_1_0:
            d_org = d_org / segments[r1].shape[0]
            d_symbolic = d_symbolic / ((no_bins -1 ) * segments_d[r1].shape[0])
        diff_d = abs(d_org - d_symbolic)
        if i == 0:
            print(f'diff_d: {diff_d:.3f} = diff_org({d_org:.3f})[\n\t{segments[r1]} - \n\t{segments[r2]}] \n- diff_symbolic({d_symbolic:.3f}) [\n\t{segments_d[r1]} - \n\t{segments_d[r2]}]')
        score += diff_d
    score = score / float(iter2)
    return score


    
